import React from 'react';
import ReactDOM from 'react-dom';
import Context from './Context';
import './index.scss';
import App from './App';
import {BrowserRouter} from "react-router-dom";

ReactDOM.render(
    <Context.Provider>
        <App/>
    </Context.Provider>,
    document.getElementById('root')
);
