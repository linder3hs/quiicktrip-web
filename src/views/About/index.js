import React, {useState} from "react"
import {makeStyles} from "@material-ui/core/styles"
import Container from "@material-ui/core/Container"
import Grid from "@material-ui/core/Grid"
import '../ExperienceDetail/index.css'
// Componente header
import Header from "../../components/Header/Header.js";
import HeaderLinks from "../../components/Header/HeaderLinks.js";
// Icon
import logo from "../../assets/img/logo.png"
import styles from "../../assets/jss/material-kit-react/views/components.js"
import classNames from "classnames"
import '../../index.scss'
import 'index.css'
import Footer from "components/Footer/Footer.js";

const useStyles = makeStyles(styles);

export default function Detail(props) {
    const classes = useStyles();
    const {...rest} = props;

    const [city, setCity] = useState("")
    const [guides, setGuides] = useState([])
    const [value, setValue] = useState(4);

    return (
        <div>
            <Header
                brand={(
                    <a href="/">
                        <img src={logo} height={50}/>
                    </a>
                )}
                rightLinks={<HeaderLinks/>}
                fixed
                color="white"
                changeColorOnScroll={{
                    height: 400,
                    color: "white"
                }}
                {...rest}
            />
            <div className={classNames(classes.main, classes.mainRaised)}>
                <div className={classes.sections}>
                    <div className={classes.container}>
                        <div className={classes.title}>
                            <br/>
                            <br/>
                            <br/>
                            <br/>
                            <br/>
                            <h1>About</h1>
                        </div>
                        <Container maxWidth="lg">
                            <Grid container spacing={3}>
                                <Grid item xs={12} sm={12} className="text-left text-justify">
                                    <h3 className="text-left">Our Philosophy</h3>
                                    <p className="text-justify">
                                        Our goal is to give you the best experiences possible with the help of what we
                                        call the protagonists of trips. They are skilled people that are in charge of
                                        making your adventure the most memorable ever, with new knowledge of newly found
                                        places you are sure to remember forever. We will make sure you find the expert
                                        in your trip.
                                    </p>
                                </Grid>
                                <Grid item xs={12} sm={12} className="text-left text-justify">
                                    <h3 className="text-left">Why should you choose Quicktrip?</h3>
                                    <p className="text-justify">
                                        - Get to know the trip expert for your next adventure
                                    </p>
                                    <p className="text-justify">
                                        - We have flexible schedules to best fit your needs
                                    </p>
                                    <p className="text-justify">
                                        - Enjoy many different types of tourism (sports, rural, adventure, culture etc)
                                    </p>
                                </Grid>
                                <Grid item xs={12} sm={12} className="text-left text-justify">
                                    <h3 className="text-left">Mission</h3>
                                    <p className="text-justify">
                                        We are the promoters of the experience to each tourist destination. We innovate
                                        together with our collaborators categorized by virtue and concordance to
                                        integrity, neatness and customer service in order to establish a solid
                                        organization. We promote ideals and create goals; leaving a social and
                                        environmental footprint.
                                    </p>
                                </Grid>
                                <Grid item xs={12} sm={12} className="text-left text-justify">
                                    <h3 className="text-left">Vision</h3>
                                    <p className="text-justify">
                                        Be a nationally and internationally recognized company that can provide the best
                                        tourism services in the coming years.
                                    </p>
                                </Grid>
                                <Grid item xs={12} sm={12} className="text-left text-justify">
                                    <h1 className="text-left">Frequently Asked Questions</h1>
                                    <h3 className="text-left">
                                        Cancel booking
                                    </h3>
                                    <p>
                                        If your activity begins in more than 24 hours, you can cancel instantly by
                                        connecting our email tourism.quicktip@gmail.com, sending the reference of your
                                        service to cancel and personal information. You will receive a full refund
                                        directly on the payment method used during the reservation.
                                    </p>
                                    <h3 className="text-left">
                                        Local partner canceled
                                    </h3>
                                    <p>
                                        If the guide of local cancels your booking, please contact us via email
                                        tourism.quicktrip@gmail.com, please contact us the form below to receive a full
                                        refund.
                                        Details: <br/>
                                        Booking reference <br/>
                                        Your name <br/>
                                        Your phone number <br/>
                                        Your message <br/>
                                    </p>
                                    <h3 className="text-left">
                                        Cancellation confirmation
                                    </h3>
                                    <p>
                                        We´ll confirm your cancellation via email. Please check your spam. Your cancellation can also be viewed in the web by page of the guide.
                                        No dude en contactarnos al correo tourism.quicktrip@gmail.com con los siguientes datos:
                                        Details: <br/>
                                        Booking reference <br/>
                                        Your name <br/>
                                        Your phone number <br/>
                                        Your message <br/>
                                    </p>
                                    <h3 className="text-left">
                                        Call customer service
                                    </h3>
                                    <p>
                                        Talk to us over the phone 24/7 <br/>
                                        Whatsapp  (+51) 945917878
                                    </p>
                                </Grid>

                            </Grid>
                        </Container>
                        <br/><br/>
                    </div>
                </div>
            </div>
            <Footer/>
        </div>

    )
}
