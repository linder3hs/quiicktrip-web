import React, {useState, useEffect} from "react"
import SideMenu from '../SideMenu'
import Container from "@material-ui/core/Container"
import Grid from "@material-ui/core/Grid"
import {makeStyles} from '@material-ui/core/styles'
import Card from "../../components/Card/Card"
import CardContent from "@material-ui/core/CardContent"
import TextField from "@material-ui/core/TextField"
import Button from "@material-ui/core/Button"
// Firebase
import firebase, {storage} from '../../service/firebaseConfig'
import Autocomplete from "@material-ui/lab/Autocomplete"
import countries from "../CreateCityAdmin/country"
import empty from "is-empty"
import ClipLoader from "react-spinners/ClipLoader"
import './index.css'
// Select
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';

const useStyles = makeStyles((theme) => ({
    toolbar: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end',
        padding: theme.spacing(0, 1),
        // necessary for content to be below app bar
        ...theme.mixins.toolbar,
    },
    content: {
        flexGrow: 1,
        padding: theme.spacing(3),
    },
    buttonExperience: {
        textAlign: 'right'
    }
}))

export default function HomeAdmin() {
    const classes = useStyles()
    const [show, setShow] = useState(false)

    // Variables
    const [name, setName] = useState("")
    const [language, setLanguage] = useState("")
    const [city, setCity] = useState("")
    const [description, setDescription] = useState("")
    const [image, setImage] = useState("")
    const [star, setStar] = useState("")
    const [listCity, setListCity] = useState([])

    const handledName = (event) => setName(event.target.value)
    const handledLanguage = (event) => setLanguage(event.target.value)
    const handledCity = (event, newValue) => setCity(newValue)
    const handledDescription = (event) => setDescription(event.target.value)
    const handleStar = event => setStar(event.target.value)

    // Upload image to firebase
    const handleImageAsFile = (e) => {
        let reader = new FileReader()
        const image = e.target.files[0]
        setShow(true)
        if (image && image.type.match('image.*')) {
            reader.readAsDataURL(image)
        }
        setTimeout(() => {
            handleFireBaseUpload(image)
        }, 2000);
    }

    const handleFireBaseUpload = (image) => {
        //e.preventDefault()
        console.log('start of upload')
        // async magic goes here...
        if (image === '') {
            console.error(`not an image, the image file is a ${typeof (image)}`)
        }

        const uploadTask = storage.ref(`/images/${image.name}`).put(image)
        uploadTask.on('state_changed',
            (snapShot) => {
                //takes a snap shot of the process as it is happening
                console.log(snapShot)
            }, (err) => {
                //catches the errors
                console.log(err)
            }, () => {
                // gets the functions from storage refences the image storage in firebase by the children
                // gets the download url then sets the image from firebase as the value for the imgUrl key:
                storage.ref('images').child(image.name).getDownloadURL()
                    .then(fireBaseUrl => {
                        setImage(fireBaseUrl)
                        console.log("image url")
                    })
                setShow(false)
            })
    }

    const createGuide = () => {
        if (!empty(name) && !empty(city) && !empty(language) && !empty(image) && !empty(description)) {
            const ref = firebase.database().ref("guide")
            ref.push({
                'name': name,
                'city': city,
                'language': language,
                'image': image,
                'decription': description,
                'star': star
            })
            setTimeout(() => {
                window.location.reload()
            }, 2000)
        } else {
            alert('Complete all fields')
        }
    }

    const getCityName = async () => {
        const ref = firebase.database().ref("city")
        await ref.on('value', snap => {
            let requests = [];
            snap.forEach(function (data) {
                const info = {
                    "name": data.val().name,
                }
                requests.push(info);
            });
            setListCity(requests)
        })
    }

    const Example = () => (
        show ? (
            <div className="flex-container">
                <div className="row">
                    <div className="flex-item">
                        <ClipLoader
                            size={150}
                            color={"#123abc"}
                            loading={show}
                        />
                    </div>
                </div>
            </div>
        ) : ""
    );


    useEffect(() => {
        getCityName()
    }, [])

    return (
        <div>
            <SideMenu/>
            <div style={{paddingTop: 40}}>
                <Container maxWidth="lg" style={{paddingTop: 20}}>
                    <Grid container spacing={3}>
                        <Card>
                            <CardContent>
                                <Grid container spacing={2}>
                                    <Grid item xs={12} sm={12}>
                                        <h2>Crear un Guia</h2>
                                    </Grid>
                                    <Grid item xs={12} sm={6}>
                                        <TextField onChange={handledName} fullWidth id="outlined-basic"
                                                   label="Nombre y Apellido" variant="outlined"/>
                                    </Grid>
                                    <Grid item xs={12} sm={6}>
                                        <TextField onChange={handledLanguage} fullWidth id="outlined-basic"
                                                   label="Idiomas seprados por comas (spanish, english, french, etc)"
                                                   variant="outlined"/>
                                    </Grid>
                                    <Grid item xs={12} sm={6}>
                                        {listCity && (
                                            <Autocomplete
                                                id="combo-box-demo"
                                                options={listCity}
                                                onInputChange={handledCity}
                                                getOptionLabel={(option) => option.name}
                                                renderInput={(params) => <TextField {...params} label="Ciudad"
                                                                                    variant="outlined"/>}
                                            />
                                        )}
                                    </Grid>
                                    <Grid item xs={12} sm={6}>
                                        <TextField onChange={handleImageAsFile} type="file" fullWidth
                                                   id="outlined-basic" variant="outlined"/>
                                    </Grid>
                                    <Grid item xs={12} sm={6}>
                                        <TextField
                                            onChange={handledDescription}
                                            fullWidth
                                            id="outlined-basic"
                                            label="Descripcion"
                                            multiline
                                            rows={4}
                                            variant="outlined"/>
                                    </Grid>
                                    <Grid item xs={12} sm={6}>
                                        <FormControl variant="outlined" style={{ width:200 }}>
                                            <InputLabel id="demo-simple-select-outlined-label">Ranking</InputLabel>
                                            <Select
                                                labelId="demo-simple-select-outlined-label"
                                                id="demo-simple-select-outlined"
                                                value={star}
                                                onChange={handleStar}
                                                label="Ranking"
                                            >
                                                <MenuItem value={1}>1</MenuItem>
                                                <MenuItem value={2}>2</MenuItem>
                                                <MenuItem value={3}>3</MenuItem>
                                                <MenuItem value={4}>4</MenuItem>
                                                <MenuItem value={5}>5</MenuItem>
                                            </Select>
                                        </FormControl>
                                    </Grid>
                                    <Example/>
                                    <Grid item xs={12} sm={12}>
                                        <Button onClick={() => createGuide()} className="btn-primary" size="large">Crear
                                            Guía</Button>
                                    </Grid>
                                </Grid>
                            </CardContent
                            >
                        </Card>
                    </Grid>
                </Container>
            </div>
        </div>
    )
}
