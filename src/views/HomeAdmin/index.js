import React from "react"
import SideMenu from '../SideMenu'
import Container from "@material-ui/core/Container"
import Grid from "@material-ui/core/Grid"
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles((theme) => ({
    toolbar: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end',
        padding: theme.spacing(0, 1),
        // necessary for content to be below app bar
        ...theme.mixins.toolbar,
    },
    content: {
        flexGrow: 1,
        padding: theme.spacing(3),
    },
}))


export default function HomeAdmin() {

    const classes = useStyles()

    return (
        <div>
            <SideMenu/>
            <main className={classes.content}>
                <div className={classes.toolbar} />
                <Container maxWidth="lg">
                    <Grid container spacing={3}>
                        <Grid item xs={12} sm={12}>
                            <h1>Hola bro</h1>
                        </Grid>
                    </Grid>
                </Container>
            </main>
        </div>
    )
}