import React, {useState} from "react"
import {makeStyles} from "@material-ui/core/styles"
import Container from "@material-ui/core/Container"
import Grid from "@material-ui/core/Grid"
import '../ExperienceDetail/index.css'
// Componente header
import Header from "../../components/Header/Header.js";
import HeaderLinks from "../../components/Header/HeaderLinks.js";
// Icon
import logo from "../../assets/img/logo.png"
import styles from "../../assets/jss/material-kit-react/views/components.js"
import classNames from "classnames"
import '../../index.scss'
import 'index.css'
import Footer from "components/Footer/Footer.js";

const useStyles = makeStyles(styles);

export default function Terms(props) {
    const classes = useStyles();
    const {...rest} = props;

    const [city, setCity] = useState("")
    const [guides, setGuides] = useState([])
    const [value, setValue] = useState(4);

    return (
        <div>
            <Header
                brand={(
                    <a href="/">
                        <img src={logo} height={50}/>
                    </a>
                )}
                rightLinks={<HeaderLinks/>}
                fixed
                color="white"
                changeColorOnScroll={{
                    height: 400,
                    color: "white"
                }}
                {...rest}
            />
            <div className={classNames(classes.main, classes.mainRaised)}>
                <div className={classes.sections}>
                    <div className={classes.container}>
                        <div className={classes.title}>
                            <br/>
                            <br/>
                            <br/>
                            <br/>
                            <br/>
                            <h1>General Terms and Conditions</h1>
                            <p>These General Terms and Conditions are subdivided into</p>
                        </div>
                        <Container maxWidth="lg">
                            <Grid container spacing={3}>
                                <Grid item xs={12} sm={12} className="text-left text-justify">
                                    <h3 className="text-left">I. Quicktrip Terms of Use</h3>
                                    <h3 className="text-left">II. General Terms and Conditions of Quicktrip, Perú
                                        (intermediary)</h3>
                                    <h3 className="text-left">III. General Terms and Conditions of the Supplier of tours
                                        and other tourist services.</h3>
                                    <p className="text-justify">
                                        Within the scope of an intermediary service, Quicktrip shall provide the
                                        visitors and Suppliers of tours or tourist services with this booking Platform.
                                        Contracts for tourist services are to be concluded directly between the users of
                                        this booking Platform and the Suppliers of tours or other tourist services. If
                                        users book services via a linked partner Platform or if a sub-intermediary makes
                                        a booking on behalf of a user of a connected partner Platform, a corresponding
                                        contract shall be concluded directly between this user and the Supplier.
                                        Quicktrip is not a contract party with regards to the tourist services offered
                                        on this booking Platform.
                                    </p>
                                </Grid>
                                <Grid item xs={12} sm={12} className="text-left text-justify">
                                    <h3 className="text-left">I. Quicktrip Terms of Use</h3>
                                    <p><strong>Who operates this website?</strong></p>
                                    <p className="text-justify">
                                        This website (including sub-sites and including text, images, videos, software,
                                        products, services and other information contained in or presented on the
                                        website; all together the "Website") is provided by QUICKTRIP, San Isidro, Lima,
                                        Perú. You can contact us by email quicktrip.peru@gmail.com or by phone using
                                        these numbers:
                                        Perú (+51)978512033 / (+51) 945917878
                                    </p>
                                </Grid>
                                <Grid item xs={12} sm={12} className="text-left text-justify">
                                    <h3 className="text-left">Application of these Terms of Use</h3>
                                    <p className="text-justify">
                                        These terms of use the "Terms of Use"), together with our privacy policy (the
                                        "Privacy Policy"), apply to any use of the Website. Visitors of the Website
                                        ("User" or "you") may use the Website only on condition that they accept the
                                        Terms of Use and read and understand the Privacy Policy. Any further use of the
                                        Website or any part of it means you have read and understood the Terms of Use
                                        and the Privacy Policy, and agree to be bound by all parts of the Terms of Use.
                                    </p>
                                </Grid>
                                <Grid item xs={12} sm={12} className="text-left text-justify">
                                    <h3 className="text-left">No offer</h3>
                                    <p className="text-justify">
                                        The information on this Website is for general informational purposes only.
                                        Information on this Website does not constitute an offer binding to us. Binding
                                        agreements with suppliers of activities available on the Website require a
                                        booking request through the Quicktrip Platform and the supplier's acceptance of
                                        the booking request according to the General Terms and Conditions of Quicktrip.
                                    </p>
                                </Grid>
                                <Grid item xs={12} sm={12} className="text-left text-justify">
                                    <h3 className="text-left">No warranty</h3>
                                    <p className="text-justify">
                                        While Quicktrip tries to ensure that the information in or through the Website
                                        is accurate, it does not provide any warranties, express or implied, in relation
                                        to its correctness, completeness, current, reliability, suitability for any
                                        purpose or otherwise (including for any information provided by third parties).
                                        Quicktrip may change, add or remove information on the Website and its structure
                                        and functions at any time at its sole discretion, without specifically informing
                                        of any such change, and without removing outdated information or characterizing
                                        it as such. Quicktrip may also block Users from accessing the Website or parts
                                        of it, or requires certain conditions to be fulfilled for such access. Quicktrip
                                        does not provide any warranties, express or implied, in relation to the
                                        availability of the Website or its functions, that the Website is free from
                                        defects, or that the Website and the infrastructure on which it runs is free
                                        from viruses and other harmful software. Moreover, Quicktrip does not guarantee
                                        that information available on the Website has not been altered through technical
                                        defects or by unauthorized third parties.
                                    </p>
                                </Grid>
                                <Grid item xs={12} sm={12} className="text-left text-justify">
                                    <h3 className="text-left">Limitation of liability</h3>
                                    <p className="text-justify">
                                        Quicktrip excludes its liability, and that of its agents and independent
                                        contractors, and its and their employees and officers, and its sub-agents or
                                        distribution partners for damages relating to your access to (or inability to
                                        access) the Website, or to any errors or omissions, or the results obtained from
                                        the use, of the Website, whatever the legal basis of such liability would be,
                                        except liability for damages caused willfully or through gross negligence, and
                                        only to the extent permitted by applicable law. Restrictions of liability do not
                                        apply within the scope of guarantees issued, in the event of an injury to life,
                                        limb or health or for claims based on product liability regulation.
                                    </p>
                                </Grid>
                                <Grid item xs={12} sm={12} className="text-left text-justify">
                                    <h3 className="text-left">Limit Quicktrip app license</h3>
                                    <p className="text-justify">
                                        Quicktrip hereby grants to you a non-exclusive, non-transferable,
                                        non-sublicensable, revocable, royalty-free, worldwide right to use the Quicktrip
                                        web according to these Terms of Use and provided that you are and will always be
                                        in compliance with these Terms of Use. You may (i) only use the Quicktrip, web
                                        in object code form and for your personal purposes (if you are a consumer) or
                                        for your internal business purposes (if you are a business); (ii) only use such
                                        number of copies of the Quicktrip web and make such number of backup copies of
                                        the Quicktrip web as may be necessary for its lawful use; (iii) not nor permit
                                        any third party to copy, adapt, reverse engineer, decompile, disassemble,
                                        modify, adapt or make error corrections to Quicktrip web in whole or in part;
                                        (iv) not rent, lease, sub-license, loan, translate, merge, adapt or modify the
                                        Quicktrip web or any associated documentation; (v) not disassemble, de-compile,
                                        reverse engineer or create derivative works based on the whole or any part of
                                        the Quicktrip web nor attempt to do any such things.
                                    </p>
                                </Grid>
                                <Grid item xs={12} sm={12} className="text-left text-justify">
                                    <h3 className="text-left">Intellectual property rights </h3>
                                    <p className="text-justify">
                                        As between you and Quicktrip, the Website is and remains protected by copyright
                                        and/or any other intellectual property rights (including protection granted
                                        through unfair competition law). You acquire no rights in the Website, and in
                                        any names, trade names, and distinctive signs of any nature (including
                                        trademarks) published on the Website. You may access and view the Website, but
                                        not incorporate it into other websites, and not copy, present, license, publish,
                                        download, upload, send or make it perceptible in any other way without our prior
                                        written consent.
                                    </p>
                                </Grid>
                                <Grid item xs={12} sm={12} className="text-left text-justify">
                                    <h3 className="text-left">Amendments to the Terms of Use</h3>
                                    <p className="text-justify">
                                        Quicktrip may amend these Terms of Use at any time and with immediate effect. If
                                        we make amendments, they apply as of the date of their publication on the
                                        Website. Quicktrip expects you to regularly refer to this section to make sure
                                        you are familiar with the applicable Terms of Use. Any further use of the
                                        Website following such amendments means you consent to the amendment.
                                    </p>
                                </Grid>
                                <Grid item xs={12} sm={12} className="text-left text-justify">
                                    <h3 className="text-left"> II. Quicktrip – User Terms and Conditions for the online
                                        offer of Quicktrip Platform.</h3>
                                    <h4>Introduction</h4>
                                    <p className="text-justify">
                                        1.1 Quicktrip approaches both, consumers and companies. For the purposes of
                                        these General Terms and Conditions, the following applies:
                                        a. A “consumer” is any individual person who has concluded a contract for
                                        purposes which cannot be predominantly assigned to commercial or independent
                                        work activities.
                                        b. A “company” is a natural person or legal entity, or a partnership, which is
                                        performing its commercial or independent activities by concluding this contract.
                                        c. The “user” denotes a natural person, unless this person has been explicitly
                                        registered with Quicktrip as a legal entity. The actions and omissions that take
                                        place during the registration of a legal entity are allocated to a natural
                                        individual, unless they are carried out within the framework of their power of
                                        attorney for the legal entity. The “user” subsequently also includes users
                                        referred by sub-agent or distribution partners.
                                    </p>
                                </Grid>
                                <Grid item xs={12} sm={12} className="text-left text-justify">
                                    <h3 className="text-left">Registration</h3>
                                    <p className="text-justify">
                                        2.1 The use of Quicktrip offer can generally be done anonymously.
                                        2.2 Certain types of use of Quicktrip Platform, such as the making of bookings,
                                        require registration. During registration, the user sends an electronic
                                        registration form and consents to the General Terms and Conditions. The
                                        registration with Quicktrip is only concluded once a confirmation is sent to the
                                        e-mail address specified by the user. Natural individuals must be over the age
                                        of 18 in order to register. The user has to keep the password he/she sets secret
                                        and take suitable precautions to prevent third parties becoming aware of it.
                                        2.3 The creation of more than one user account for the same natural individual
                                        or legal entity is not permitted. The user account cannot be transferred.
                                    </p>
                                </Grid>
                                <Grid item xs={12} sm={12} className="text-left text-justify">
                                    <h3 className="text-left">Services of Quicktrip</h3>
                                    <p className="text-justify">
                                        3.1 Once the user has entered his/her desired service on the Quicktrip Platform
                                        (e.g. travel destination, type of tour, start time, number of participants &
                                        price options), Quicktrip shall show the user the information about the
                                        Suppliers’ services (“service information “). Based on this information, the
                                        user can, where applicable after checking availability, make a contractual
                                        offer, by placing an offer, to be sent to the respective Supplier, whereby the
                                        sub-intermediates act on behalf of the referred user. This happens, after the
                                        selection and transfer to the shopping basket, by clicking on the button
                                        “confirm and book”.
                                        3.2 The user is bound to his/her binding offer for five working days. For more
                                        information see Section 5 of the General Terms and Conditions of the Supplier of
                                        tours and other tourist services in Part III.
                                        3.3 Quicktrip shall notify the user of the conditions of transport and business
                                        of the Supplier for their contractual relationship with the user and for their
                                        services. They can be found in the respective tenders. The user is responsible
                                        for meeting and complying with these conditions. The Supplier reserves the right
                                        to not allow the user to undertake an action, or to exclude it, if they do not
                                        meet the conditions. In this case the paid price shall not be refunded.
                                        3.4 Quicktrip shall provide the user with a booking confirmation issued in the
                                        name of, and on behalf of, the Supplier, as well as a payment confirmation. The
                                        use of the Quicktrip Platform itself is essentially free of charge for the user.
                                        The costs for the technical access to the Quicktrip Platform (e.g. internet
                                        access) are to be borne by the user. Quicktrip is permitted to collect the
                                        invoiced amounts in the name of, and behalf of the Supplier.
                                        3.5 Quicktrip forward the user any data for the use of a Supplier’s service
                                        according to the applicable conditions (such as ticket data), once they have
                                        been received by the Supplier.
                                        3.6 Quicktrip assumes no guarantee for the accuracy of forwarded data, or for
                                        the performance of services by the Supplier, as all the information indicated
                                        and forwarded is based on data from Suppliers or third parties, which Quicktrip
                                        cannot check in detail.
                                    </p>
                                </Grid>
                                <Grid item xs={12} sm={12} className="text-left text-justify">
                                    <h3 className="text-left">Payments on Quicktrip</h3>
                                    <p className="text-justify">
                                        4.1 The service agreement concluded with the Supplier shall apply to the fees to
                                        be paid by the user for the services of the Supplier.
                                        4.2 Quicktrip is permitted to collect the invoiced amounts in the name of and on
                                        behalf of the Supplier, provided nothing else has been explicitly stated in the
                                        Supplier’s invoice. If claims have to be paid by the user in a different
                                        currency than its national currency (claims for payment in foreign currencies),
                                        Quicktrip can demand payment in the national currency of the user and the
                                        foreign currency claim can be converted based on the current exchange rate at
                                        the time the contract is concluded. Quicktrip can charge the user a suitable
                                        conversion charge for this.
                                        4.3 The contact partner, and contract partner, of the user in connection with
                                        the service agreement and its payment, is the respective Supplier. The user can
                                        only assert the repayment of a payment to the respective Supplier. A refund
                                        granted by the Supplier can also be processed by the Supplier via Quicktrip. To
                                        simplify the process for the user, communication via Quicktrip Platform is
                                        recommended.
                                        4.4 To use the payment functions of Quicktrip, the user has to register. The
                                        user has to enter correct payment information and update the details immediately
                                        in the event of changes. Quicktrip can reject the payment method specified by
                                        the user. The user will be notified of the payment methods permitted for the
                                        respective service during the order process7. The best price guarantee of
                                        Quicktrip.
                                    </p>
                                </Grid>
                                <Grid item xs={12} sm={12} className="text-left text-justify">
                                    <h3 className="text-left">Duties and obligations of the user</h3>
                                    <p className="text-justify">5.1 The user shall keep the registration data (user
                                        login and password) secret and not allow third parties access to Quicktrip
                                        Portal using his/her registration data. The user shall be accountable for all
                                        use of his/her user account on Quicktrip Portal.
                                        5.2 After receiving the service information, the user can send any orders to
                                        Quicktrip for forwarding to the Supplier.
                                        5.3 The user shall exempt Quicktrip from third-party claims based on his/her use
                                        of the Quicktrip Platform, unless they are the fault of Quicktrip.
                                    </p>
                                </Grid>
                                <Grid item xs={12} sm={12} className="text-left text-justify">
                                    <h3 className="text-left">Availability and warranty</h3>
                                    <p className="text-justify">
                                        6.1There is no claim for availability, quality or
                                        service features, or technical support for the Quicktrip. Platform. Quicktrip
                                        can redesign, reduce or suspend their online portal Quicktrip at any time, at
                                        its discretion. Existing agreements of the user with a Supplier, as well as the
                                        execution of these agreements, remain unaffected by these changes.
                                        6.2 Quicktrip makes no guarantee or warranty for the accuracy or completeness of
                                        data provided by third parties (such as Suppliers).
                                        6.3 Quicktrip makes no guarantee or warranty for the services provided by the
                                        Suppliers. The contact partner of the user in the event of questions and claims
                                        in connection with a service agreement and its execution is the respective
                                        Supplier.
                                    </p>
                                </Grid>
                                <Grid item xs={12} sm={12} className="text-left text-justify">
                                    <h3 className="text-left">Liability of Quicktrip</h3>
                                    <p className="text-justify">
                                        7.1 If Quicktip has not assumed a corresponding contractual obligation by means
                                        of an explicit agreement with the user, it is not liable for the realization of
                                        corresponding agreements with Suppliers in line with the booking request of the
                                        user.
                                        7.2 Without explicit agreement or an assurance of this kind, QUICKTRIP is not
                                        liable for defects in the performance of the service and personal or material
                                        damage incurred by the user in connection with the travel service provided,
                                        concerning the services provided.
                                        7.3 Any liability QUICKTRIP due to the culpable violation of obligations in the
                                        brokering of contracts remains unaffected by the aforementioned conditions.
                                        7.4 The liability of QUICKTRIP for contractual claims of the user is limited to
                                        three-times the price of the tourist services procured, except for
                                        • any violation of a key obligation, which needs to be fulfilled in order to
                                        allow the proper execution of the brokering agreement or the violation of which
                                        endangers the fulfilment of the contractual purpose
                                        • liability for damage incurred by the user due to the injury to life, limb or
                                        health, which is based on a negligent breach of duty by Quicktrip or a vicarious
                                        agent of Quicktrip.
                                        • liability of Quicktrip for other damage incurred by the user due to a
                                        grossly-negligent breach of duty by Quicktrip or a willful or grossly-negligent
                                        breach of duty by a legal representative or vicarious agent of Quicktrip.
                                    </p>
                                </Grid>
                                <Grid item xs={12} sm={12} className="text-left text-justify">
                                    <h3 className="text-left">Data protection</h3>
                                    <p className="text-justify">
                                        9.1 Quicktrip collects and uses personal data of users to the extent that is
                                        necessary for the creation, design of content or modification of the contractual
                                        conditions for Quicktrip between the user and Quicktrip.
                                        9.2 If Quicktrip is involved in the communication for a service agreement
                                        between the user and the respective Supplier, it shall transfer the data
                                        required for this agreement to the respective Supplier. This Supplier processes
                                        and uses the data to initiate, conclude and execute the contract on its own
                                        responsibility. The identity of the respective Supplier can be taken from the
                                        booking dialogue.
                                    </p>
                                </Grid>
                                <Grid item xs={12} sm={12} className="text-left text-justify">
                                    <h3 className="text-left">Termination</h3>
                                    <p className="text-justify">
                                        Users can cancel their registration on Quicktrip Portal at any time by blocking
                                        their user account. Quicktrip can cancel a registration unilaterally with one
                                        week’s notice. Claims which have arisen before this is done, remain unaffected.
                                        The right to extraordinary cancellation remains unaffected.
                                    </p>
                                </Grid>
                                <Grid item xs={12} sm={12} className="text-left text-justify">
                                    <h3 className="text-left">Changes to these general Terms and Conditions</h3>
                                    <p className="text-justify">
                                        10.1 Quicktrip reserves the right to change these General Terms and Conditions
                                        at any time and without stating the reasons. Quicktrip will only make changes
                                        affecting the user, which the user must agree to in consideration of mutual
                                        interests. This concerns, for example, cases where the equivalence principle has
                                        been disturbed, as well as loopholes and changes in legislation. The amended
                                        conditions will be sent to the users by e-mail at the latest two weeks before
                                        they come into effect. If a user does not reject the validity of the new General
                                        Terms and Conditions within two weeks of the receipt of the e-mail, the amended
                                        General Terms and Conditions are deemed to have been accepted. Quicktrip will
                                        notify the user, in the e-mail containing the amended conditions, of the
                                        importance of this two-week deadline.
                                        10.2 These Terms and Conditions can be amended at any time and without notice
                                        for future intermediation of contracts with Supplier. The respective conditions
                                        valid for each individual contract conclusion shall apply.
                                    </p>
                                </Grid>
                                <Grid item xs={12} sm={12} className="text-left text-justify">
                                    <h1 className="text-left">III. General Terms and Conditions of the Supplier of tours
                                        and other tourist services</h1>
                                    <h3 className="text-left">Introduction</h3>
                                    <p className="text-justify">
                                        Part III of these General Terms and Conditions applies to all contracts
                                        concluded between the Suppliers and the users via the Quicktrip Platform. This
                                        also applies, if access to the Quicktrip Platform is provided via a cooperating
                                        partner (sub- agent, distribution partner).
                                    </p>
                                    <h3 className="text-left">Arrival at meeting point and compliance with
                                        condition</h3>
                                    <p className="text-justify">
                                        You are responsible for arriving on time at the notified meeting point. If you
                                        are travelling to an activity from abroad, you are responsible for having the
                                        necessary travel documents (passport etc.), and for observing the health
                                        regulations etc.
                                    </p>
                                    <h3 className="text-left">Payment</h3>
                                    <p className="text-justify">
                                        The agreed total price for the service we have provided is due upon the
                                        conclusion
                                        of the contract.
                                    </p>
                                    <h3 className="text-left">Cancellation guidelines</h3>
                                    <p className="text-justify">
                                        4.1 If you reject (cancel) the activity, the cancellation conditions stated in
                                        the product description as well as on your voucher apply. Quicktrip advises the
                                        user to carefully read the information in the product description.
                                        4.2 Provided there are no deviating cancellation conditions in the product
                                        description of your Supplier, the following cancellation fees will be charged by
                                        the Supplier of the tourist services:
                                        a. Up to 24 hours before the start of the activity: full refund
                                        b. Less than 24 hours before the start of the activity or in the event of a
                                        no-show: no refund
                                        4.3 You, or the user you have referred, are free to prove to the Supplier that
                                        they have not incurred any damage, or substantially less damage than the fee
                                        charged by the Supplier.
                                        4.4 The Supplier reserves the right to request higher, specific compensation
                                        instead of the aforementioned flat-rate fees, if the Supplier can prove that it
                                        has incurred much higher expenses than the respectively applicable flat-rate
                                        fee. In this case the Supplier is obliged to provide specific figures and
                                        evidence of the compensation requested, taking into account the saved expenses
                                        and any other use of the services.
                                        4.5 The refund is done using the same payment method. In the event of credit
                                        cards which are charged monthly, the amount shall be credited at the end of the
                                        current invoicing period. The exact time of the refund depends on the user’s
                                        credit card agreement. The refund by bank transfer shall be done within seven
                                        bank working days.
                                    </p>
                                    <h3 className="text-left">Extraordinary cancellation</h3>
                                    <p className="text-justify">
                                        The Supplier can cancel the activity on the agreed date without observing a
                                        period of notice, if weather conditions, official measures, strikes or other
                                        unforeseeable or unavoidable external conditions (in particular force majeure)
                                        make the execution of the activity impossible, make it considerably more
                                        difficult or endanger it. In this case the paid price is refunded.
                                    </p>
                                    <h3 className="text-left">Exclusion of participation </h3>
                                    <p className="text-justify">
                                        The Supplier is permitted not to allow you to join an activity, or to exclude
                                        you from one, if you do not meet the personal participation requirements, your
                                        participation would endanger you or someone else or in any other way make the
                                        activity impossible in the long-term. This applies accordingly to a user you
                                        have referred. In these cases, the paid price cannot be refunded.
                                    </p>
                                    <h3 className="text-left">Futher conditions</h3>
                                    <p className="text-justify">
                                        Further conditions or deviating conditions can be found in the respective
                                        tenders.
                                    </p>
                                    <h3 className="text-left">Change to these general terms and conditions</h3>
                                    <p className="text-justify">
                                        These Terms and Conditions of the Supplier of tours and other tourist services
                                        of Part III can be amended at any time and without notice for future bookings.
                                        The respective conditions valid for each individual booking shall apply. The
                                        user has no claim for future bookings based on the existing conditions
                                    </p>
                                    <h3 className="text-left">Exclusion of participation</h3>
                                    <p className="text-justify">
                                        The Supplier is permitted not to allow you to join an activity, or to exclude
                                        you from one, if you do not meet the personal participation requirements, your
                                        participation would endanger you or someone else or in any other way make the
                                        activity impossible in the long-term. This applies accordingly to a user you
                                        have referred. In these cases, the paid price cannot be refunded.
                                    </p>
                                    <h3 className="text-left">Changes to the program</h3>
                                    <p className="text-justify">
                                        Furthermore, the Supplier reserves the right to make non-essential changes to
                                        the program, if this is necessary due to unforeseeable or unavoidable
                                        conditions.
                                    </p>
                                </Grid>
                            </Grid>
                        </Container>
                        <br/><br/>
                    </div>
                </div>
            </div>
            <Footer/>
        </div>
    )
}
