import React, {useState, useEffect} from "react"
import {makeStyles} from "@material-ui/core/styles"
import Container from "@material-ui/core/Container"
import Grid from "@material-ui/core/Grid"
import Card from '@material-ui/core/Card'
import CardMedia from "@material-ui/core/CardMedia"
import '../ExperienceDetail/index.css'
// Images
import p1 from '../../assets/img/p1.jpg'
import p2 from '../../assets/img/p2.jpg'
import p3 from '../../assets/img/p3.jpg'
import p4 from '../../assets/img/p4.jpg'
import p5 from '../../assets/img/p5.jpg'
// Componente header
import Header from "../../components/Header/Header.js";
import HeaderLinks from "../../components/Header/HeaderLinks.js";
import Link from "@material-ui/core/Link";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardContent from "@material-ui/core/CardContent";
// Icon
import logo from "../../assets/img/logo.png"
import Parallax from "../../components/Parallax/Parallax"
import GridContainer from "../../components/Grid/GridContainer"
import GridItem from "../../components/Grid/GridItem"
import styles from "../../assets/jss/material-kit-react/views/components.js"
import classNames from "classnames"
// Firebase
import firebase from '../../service/firebaseConfig'
import TextTruncate from "react-text-truncate";
import '../../index.scss'
import Footer from "components/Footer/Footer.js";
import BeautyStars from 'beauty-stars';

const useStyles = makeStyles(styles);

export default function Detail(props) {
    const classes = useStyles();
    const {...rest} = props;

    const [city, setCity] = useState("")
    const [guides, setGuides] = useState([])
    const [value, setValue] = useState(4);

    const getCityByName = async () => {
        const ref = firebase.database().ref("city")
        const nameFromRoute = (new URLSearchParams(window.location.search)).get("city")
        console.log("name " + nameFromRoute)
        await ref.on('value', snap => {
            console.log(nameFromRoute)
            console.log("sadasda")
            let requests = [];
            snap.forEach(function (data) {
                if (data.val().name === nameFromRoute) {
                    console.log(data.val().name)
                    const info = {
                        "key": data.key,
                        "country": data.val().country,
                        "decription": data.val().decription,
                        "image": data.val().image,
                        "name": data.val().name,
                    }
                    requests.push(info);
                }
            });
            console.log(requests)
            setCity(requests[0])
        })
    }

    const getGuidesByCity = async () => {
        const ref = firebase.database().ref("guide")
        const nameFromRoute = (new URLSearchParams(window.location.search)).get("city")
        await ref.on('value', snap => {
            let requests = [];
            snap.forEach(function (data) {
                if (data.val().city === nameFromRoute) {
                    const info = {
                        "key": data.key,
                        "city": data.val().city,
                        "star": data.val().star,
                        "decription": data.val().decription,
                        "image": data.val().image,
                        "name": data.val().name,
                        "language": data.val().language,
                    }
                    requests.push(info);
                }

            });
            console.log(requests)
            setGuides(requests)
        })
    }

    useEffect(() => {
        getCityByName()
        getGuidesByCity()
    }, [])

    return (
        <div>
            <Header
                brand={(
                    <a href="/">
                        <img src={logo} height={50}/>
                    </a>
                )}
                rightLinks={<HeaderLinks/>}
                fixed
                color="white"
                changeColorOnScroll={{
                    height: 400,
                    color: "white"
                }}
                {...rest}
            />
            <Parallax image={city?.image} style={{height: '60vh'}}>
                <div className={classes.container}>
                    <GridContainer>
                        <GridItem>
                            <div className={classes.brand}>
                                <h1 className={classes.title}>{city?.name}</h1>
                                <TextTruncate
                                    line={7}
                                    element="span"
                                    truncateText="…"
                                    text={city?.decription}
                                />
                            </div>
                        </GridItem>
                    </GridContainer>
                </div>
            </Parallax>
            <div className={classNames(classes.main, classes.mainRaised)}>
                <div className={classes.sections}>
                    <div className={classes.container}>
                        <div className={classes.title}>
                            <h5 style={{fontWeight: 'bold', fontSize: 22, marginTop: 30}}>Tour Guides
                                of {city?.name}</h5>
                        </div>
                        <Container maxWidth="lg">
                            <Grid container spacing={3}>
                                {guides && (
                                    guides.map(guide => (
                                        <Grid item xs={12} lg={3} md={4} sm={3}>
                                            <Card className="card-content">
                                                <Link href={"/detail/?guide=" + guide.name + "-" + guide.city}
                                                      style={{textDecoration: "none", color: '#222222'}}>
                                                    <CardActionArea>
                                                        <CardMedia
                                                            component="img"
                                                            alt="Contemplative Reptile"
                                                            height="270"
                                                            image={guide.image}
                                                            title="Contemplative Reptile"
                                                        />
                                                        <CardContent>
                                                            <p className="text-primary">{guide.name}</p>
                                                            <TextTruncate
                                                                line={7}
                                                                element="span"
                                                                truncateText="…"
                                                                text={guide.decription}
                                                            />
                                                            <div className="top-20">
                                                                <p className="text-primary">Languages</p>
                                                                <p className="text-seconday">{guide.language}</p>
                                                            </div>
                                                            <BeautyStars size={20} value={guide.star}/>
                                                        </CardContent>
                                                    </CardActionArea>
                                                </Link>
                                            </Card>
                                        </Grid>
                                    ))
                                )}
                            </Grid>
                        </Container>
                        <br/><br/>
                    </div>
                </div>
            </div>
            <Footer/>
        </div>

    )
}
