import React, { useState } from "react"
import Container from "@material-ui/core/Container"
import Grid from "@material-ui/core/Grid"
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'
import Hidden from '@material-ui/core/Hidden'
import landscappe from '../../assets/img/landscape.jpg'
import logo from  '../../assets/img/logo.png'
import './index.css'

export default function Login() {

    return (
        <Container maxWidth="lg" spacing={3}>
            <Grid container direction={"row"} spacing={5}>
                <Grid item sm={4} xs={12}>
                    <img className="image-logo" src={logo} width={200} height={130} alt=""/>
                    <h2>Login</h2>
                    <Grid item>
                        <TextField fullWidth label="Usuario" variant="outlined" />
                    </Grid>
                    <br/>
                    <Grid item>
                        <TextField fullWidth label="Password" variant="outlined" />
                    </Grid>
                    <br/>
                    <Grid item style={{ textAlign:'right' }}>
                        <Button fullWidth variant="contained" color="primary">
                            Login
                        </Button>
                        <span>Aun no tienes un cuenta <a style={{ textDecoration: 'underline' }} href="/register">Registrate</a></span>
                    </Grid>
                    <br/>
                    <Grid item style={{ textAlign:'center' }}>
                        <Button fullWidth variant="contained" color="secondary">
                            Login con Google
                        </Button>
                    </Grid>
                </Grid>
                <Hidden xsDown>
                    <Grid item sm={8} xs={12}>
                        <img className="image-bg" src={landscappe} />
                    </Grid>
                </Hidden>
            </Grid>
        </Container>
    )
}