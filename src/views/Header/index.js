import React, {useState} from 'react'
import {makeStyles} from '@material-ui/core/styles'
import MenuIcon from '@material-ui/icons/Menu';
import './index.css'
import '../../index.scss'
import logo from '../../assets/img/logo-white.png'
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        backGroundColor: '#000'
    },
    menuButton: {
        flexGrow: 1,
        marginRight: theme.spacing(5),
    },
    title: {
        flexGrow: 1,
    },
}));

export default function Header() {
    const classes = useStyles()
    const [open, setOpen] = React.useState(false);

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    return (
        <div className={classes.root}>
            <div className="bg-header">
                <div className="content-header">
                    <div className="block-inline">
                        <img height={30} width={100} src={logo} alt="logo" />
                    </div>
                    <div className="block-inline">
                        <div className="content-text-header">
                            <span className="text-white text-header">Home</span>
                            <span className="text-white text-header">About/FAQ</span>
                            <span className="text-white text-header">Sig In</span>
                            <span className="text-white text-header">Sign Up</span>
                        </div>
                        <div className="content-header-responsive">
                            <a onClick={handleClickOpen}><MenuIcon style={{ color:"white" }}/></a>
                        </div>
                    </div>
                </div>
            </div>
            <Dialog
                open={open}
                onClose={handleClose}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogContent className="content-dialog">
                    <DialogContentText id="alert-dialog-description">
                        <span>Home</span><br/>
                        <span>About/FAQ</span><br/>
                        <span>Sig In</span><br/>
                        <span>Sign Up</span><br/>
                    </DialogContentText>
                </DialogContent>
            </Dialog>
        </div>
    )
}
