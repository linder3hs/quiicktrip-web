import React, {useState, useEffect} from "react"
import SideMenu from '../SideMenu'
import Container from "@material-ui/core/Container"
import Grid from "@material-ui/core/Grid"
import {makeStyles} from '@material-ui/core/styles';
import Card from "../../components/Card/Card"
import CardContent from "@material-ui/core/CardContent"
import Button from "@material-ui/core/Button";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardMedia from "@material-ui/core/CardMedia";
import CardActions from "@material-ui/core/CardActions";
import Avatar from "@material-ui/core/Avatar"
import Divider from "@material-ui/core/Divider"
import {Link} from "react-router-dom";
// Firebase
import firebase from '../../service/firebaseConfig'
import './index.css'
import TextTruncate from 'react-text-truncate'; // recommend

const useStyles = makeStyles((theme) => ({
    toolbar: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end',
        padding: theme.spacing(0, 1),
        // necessary for content to be below app bar
        ...theme.mixins.toolbar,
    },
    content: {
        flexGrow: 1,
        padding: theme.spacing(3),
    },
}))


export default function HomeAdmin() {

    const classes = useStyles()
    const [listExperience, setListExperience] = useState([])

    const getListExperience = async () => {
        const ref = firebase.database().ref("experience")
        await ref.on('value', snap => {
            let experiences = []
            snap.forEach(function (data) {
                const info = {
                    "key": data.key,
                    "city": data.val()?.city,
                    "description": data.val()?.description,
                    "guide": data.val()?.guide,
                    "title": data.val()?.title,
                    "connection": data.val()?.connection,
                    "imageOne": data.val()?.imageOne,
                    "imageTwo": data.val()?.imageTwo,
                    "imageThree": data.val()?.imageThree,
                    "imageFour": data.val()?.imageFour,
                    "video": data.val()?.video
                }
                experiences.push(info)
            })
            setListExperience(experiences)
        })
    }

    const deleteExperience = (key) => firebase.database().ref("experience/" + key).remove()

    useEffect(() => {
        getListExperience()
    }, [])

    return (
        <div>
            <SideMenu/>
            <main className={classes.content}>
                <div className={classes.toolbar}/>
                <Container maxWidth="lg">
                    <Grid container spacing={3}>
                        <Grid item xs={7} lg={6} md={6} sm={6}>
                            <h3 className="text-left">Lista de Experiencias</h3>
                        </Grid>
                        <Grid item sm={6} xs={5} style={{textAlign: 'right', paddingTop: 24}}>
                            <Link to="/create-experience">
                                <Button className="btn-primary" size="large">Create Experiencie</Button>
                            </Link>
                        </Grid>
                        {
                            listExperience && (
                                listExperience.map(experience => (
                                    <Grid item xs={12} lg={3} md={4} sm={3}>
                                        <Card className="card-content">
                                            <CardActionArea>
                                                <CardMedia
                                                    component="img"
                                                    alt="Contemplative Reptile"
                                                    height="240"
                                                    image={experience.imageOne}
                                                    title="Contemplative Reptile"
                                                />
                                                <CardContent>
                                                    <h6>{experience.title}</h6>
                                                    <TextTruncate
                                                        line={5}
                                                        element="span"
                                                        truncateText="…"
                                                        text={ experience.description }
                                                    />
                                                    <Grid container>
                                                        <Grid item sm={8}>
                                                            <h5>{experience.guide}</h5>
                                                        </Grid>
                                                    </Grid>
                                                </CardContent>
                                            </CardActionArea>
                                            <CardActions>
                                                <Button onClick={ () => deleteExperience(experience.key)} size="small" color="primary">
                                                    Eliminar
                                                </Button>
                                                <Link to={"/edit-experience?key=" + experience.key}>
                                                    <Button size="small" color="primary">
                                                        Editar
                                                    </Button>
                                                </Link>
                                            </CardActions>
                                        </Card>
                                    </Grid>
                                ))
                            )
                        }
                    </Grid>
                </Container>
            </main>
        </div>
    )
}
