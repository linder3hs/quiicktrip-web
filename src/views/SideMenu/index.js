import React from 'react'
import {makeStyles} from '@material-ui/core/styles'
import MenuIcon from '@material-ui/icons/Menu'
import '../Header/index.css'
import logo from '../../assets/img/logo-white.png'
import Dialog from '@material-ui/core/Dialog'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
// Router
import { Link } from 'react-router-dom'

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        backGroundColor: '#000'
    },
    menuButton: {
        flexGrow: 1,
        marginRight: theme.spacing(5),
    },
    title: {
        flexGrow: 1,
    },
}));

export default function MiniDrawer() {

    const classes = useStyles()
    const [open, setOpen] = React.useState(false);

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    return (
        <div className={classes.root}>
            <div className="bg-header">
                <div className="content-header">
                    <div className="block-inline">
                        <img height={30} width={100} src={logo} alt="logo" />
                    </div>
                    <div className="block-inline">
                        <div className="content-text-header">
                            <Link to="/list-city" style={{ backGroundColor: '#000' }}>
                                <span className="text-white text-header">City</span>
                            </Link>
                            <Link to="/list-admin-guide">
                                <span className="text-white text-header">Guides</span>
                            </Link>
                            <Link to="/list-experience">
                                <span className="text-white text-header">Experience</span>
                            </Link>
                        </div>
                        <div className="content-header-responsive">
                            <a onClick={handleClickOpen}><MenuIcon style={{ color:"white" }}/></a>
                        </div>
                    </div>
                </div>
            </div>
            <Dialog
                open={open}
                onClose={handleClose}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogContent className="content-dialog">
                    <DialogContentText id="alert-dialog-description">
                        <Link to="/list-city" style={{ backGroundColor: '#000' }}>
                            <span>City</span><br/>
                        </Link>
                        <Link to="/list-admin-guide">
                            <span>Guides</span><br/>
                        </Link>
                        <Link to="/list-experience">
                            <span>Experience</span><br/>
                        </Link>
                    </DialogContentText>
                </DialogContent>
            </Dialog>
        </div>
    );
}
