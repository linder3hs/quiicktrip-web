import React from "react";
// nodejs library that concatenates classes
import classNames from "classnames";
// react components for routing our app without refresh
import {Link} from "react-router-dom";
// @material-ui/core components
import {makeStyles} from "@material-ui/core/styles";
// @material-ui/icons
// core components
import Header from "components/Header/Header.js";
import Footer from "components/Footer/Footer.js";
// sections for this page
import HeaderLinks from "components/Header/HeaderLinks.js";
import SectionBasics from "./Sections/SectionBasics.js";

import styles from "assets/jss/material-kit-react/views/components.js";
import logo from '../../assets/img/logo.png'
import {Slide} from 'react-slideshow-image'
import '../../index.css'

const useStyles = makeStyles(styles);

const slideImages = [
    'https://firebasestorage.googleapis.com/v0/b/quicktrip-64366.appspot.com/o/banner%2FLaguna%20Paron%20-%20Ancash.jpg?alt=media&token=d378abbe-610c-4582-851d-1a8e599dfc04',
    'https://firebasestorage.googleapis.com/v0/b/quicktrip-64366.appspot.com/o/banner%2Fbg1.jpg?alt=media&token=47996fce-0ead-480c-9515-474eecad3146',
    'https://firebasestorage.googleapis.com/v0/b/quicktrip-64366.appspot.com/o/banner%2Frsz_machu_picchu_2_-_cusco.jpg?alt=media&token=bd40fa30-06c9-4364-98bd-40c80abc9b7d',
    'https://firebasestorage.googleapis.com/v0/b/quicktrip-64366.appspot.com/o/banner%2Fbg2.jpg?alt=media&token=c3fdb92a-ccfd-4d93-81a3-fc4cee56698f'
];

const properties = {
    duration: 5000,
    transitionDuration: 500,
    infinite: true,
    indicators: true,
    arrows: true,
    pauseOnHover: true,
    onChange: (oldIndex, newIndex) => {
        console.log(`slide transition from ${oldIndex} to ${newIndex}`);
    }
}

export default function Components(props) {
    const classes = useStyles();
    const {...rest} = props;

    return (
        <div>
            <Header
                brand={(
                    <a href="/">
                        <img src={logo} height={50}/>
                    </a>
                )}
                rightLinks={<HeaderLinks/>}
                fixed
                color="white"
                changeColorOnScroll={{
                    height: 400,
                    color: "white"
                }}
                {...rest}
            />
            <Slide {...properties}>
                <div className="each-slide">
                    <div style={{
                        'backgroundImage': `url(${slideImages[0]})`,
                        backgroundPosition: 'center center',
                        backgroundRepeat: 'no-repeat'
                    }}>
                        <span>
                            <h1 className={classes.title}>Find the expert in your trip.</h1>
                            <h5 className={classes.subtitle}>
                                Unique virtual experiences led by local tour guides.
                            </h5>
                            <br/>
                            <br/>
                            <br/>
                            <h4>Laguna Parón, Ancash</h4>
                        </span>
                    </div>
                </div>
                <div className="each-slide">
                    <div style={{
                        'backgroundImage': `url(${slideImages[1]})`,
                        backgroundPosition: 'center center',
                        backgroundRepeat: 'no-repeat'
                    }}>
                        <span>
                            <h1 className={classes.title}>Find the expert in your trip.</h1>
                            <h5 className={classes.subtitle}>
                               Unique virtual experiences led by local tour guides.
                            </h5>
                             <br/>
                            <br/>
                            <br/>
                            <h4> Cañon de Colca, Arequipa</h4>
                        </span>
                    </div>
                </div>
                <div className="each-slide">
                    <div style={{
                        'backgroundImage': `url(${slideImages[2]})`,
                        backgroundPosition: 'center center',
                        backgroundRepeat: 'no-repeat'
                    }}>
                         <span>
                            <h1 className={classes.title}>Find the expert in your trip.</h1>
                            <h5 className={classes.subtitle}>
                                Unique virtual experiences led by local tour guides.
                            </h5>
                              <br/>
                            <br/>
                            <br/>
                            <h4> Machu Picchu</h4>
                        </span>
                    </div>
                </div>
                <div className="each-slide">
                    <div style={{
                        'backgroundImage': `url(${slideImages[3]})`,
                        backgroundPosition: 'center center',
                        backgroundRepeat: 'no-repeat'
                    }}>
                         <span>
                            <h1 className={classes.title}>Find the expert in your trip.</h1>
                            <h5 className={classes.subtitle}>
                                Unique virtual experiences led by local tour guides.
                            </h5>
                                <br/>
                            <br/>
                            <br/>
                            <h4> Montaña de Siete Colores, Cusco</h4>
                        </span>
                    </div>
                </div>
            </Slide>

            {/*<Parallax*/}
            {/*    image={"https://firebasestorage.googleapis.com/v0/b/quicktrip-64366.appspot.com/o/banner%2Fbg4.jpg?alt=media&token=0286d98f-832f-4693-9be0-e46133f6475f"}>*/}
            {/*    <div className={classes.container}>*/}
            {/*        <GridContainer>*/}
            {/*            <GridItem>*/}
            {/*                <div className={classes.brand}>*/}
            {/*                    <p className={classes.subtitle} style={{fontWeight: 'bold'}}>WE PRESENT</p>*/}
            {/*                    <h1 className={classes.title}>Find the expert in your trip.</h1>*/}
            {/*                    <h5 className={classes.subtitle}>*/}
            {/*                        Unique virtual experiences hosted by professionals in all things tourism.*/}
            {/*                    </h5>*/}
            {/*                    <h5 className={classes.subtitle}>*/}
            {/*                        Unique virtual experiences led by local tour guides.*/}
            {/*                    </h5>*/}
            {/*                </div>*/}
            {/*            </GridItem>*/}
            {/*        </GridContainer>*/}
            {/*    </div>*/}
            {/*</Parallax>*/}

            <div className={classNames(classes.main, classes.mainRaised)}>
                <SectionBasics/>
            </div>
            <Footer/>
        </div>
    );
}
