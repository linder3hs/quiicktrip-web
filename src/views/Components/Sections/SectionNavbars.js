import React from "react"
// @material-ui/core components
import {makeStyles} from "@material-ui/core/styles"

import styles from "assets/jss/material-kit-react/views/componentsSections/navbarsStyle.js"
import Card from "@material-ui/core"

const useStyles = makeStyles(styles);

export default function SectionNavbars() {
    const classes = useStyles();
    return (
        <div className={classes.section}>
            <div className={classes.container}>
                <div className={classes.title}>
                    <h3>Navigation</h3>
                </div>
            </div>
        </div>
    );
}
