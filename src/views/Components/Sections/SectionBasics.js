import React, { useState, useEffect } from "react"
import {makeStyles} from "@material-ui/core/styles"
import styles from "../../../assets/jss/material-kit-react/views/componentsSections/basicsStyle"
import '../../../index.scss'
// Material ui
import Container from "@material-ui/core/Container"
import Grid from "@material-ui/core/Grid"
import Card from '@material-ui/core/Card'
import CardActionArea from '@material-ui/core/CardActionArea'
import CardContent from '@material-ui/core/CardContent'
import CardMedia from '@material-ui/core/CardMedia'
import Link from "@material-ui/core/Link"
// Icons
import AvTimerIcon from '@material-ui/icons/AvTimer';
import MonetizationOnIcon from '@material-ui/icons/MonetizationOn'
import RoomIcon from '@material-ui/icons/Room'
import AccessTimeIcon from '@material-ui/icons/AccessTime'
import SecurityIcon from '@material-ui/icons/Security';
import DateRangeIcon from '@material-ui/icons/DateRange';

import firebase from "../../../service/firebaseConfig";
import TextTruncate from 'react-text-truncate'; // recommend

import Carousel from 'react-multi-carousel';
import 'react-multi-carousel/lib/styles.css';
import MessengerCustomerChat from "react-messenger-customer-chat";
import Context from "../../../Context";

const useStyles = makeStyles(styles)

export default function SectionBasics() {
    const classes = useStyles();

    const [cities, setCities] = useState([])

    const getCities = async () => {
        const ref = firebase.database().ref("city")
        await ref.on('value', snap => {
            let requests = [];
            snap.forEach(function (data) {
                const info = {
                    "key": data.key,
                    "country": data.val().country,
                    "decription": data.val().decription,
                    "image": data.val().image,
                    "name": data.val().name,
                }
                requests.push(info);
            });
            setCities(requests)
        })
    }

    const responsive = {
        superLargeDesktop: {
            // the naming can be any, depends on you.
            breakpoint: { max: 4000, min: 3000 },
            items: 5
        },
        desktop: {
            breakpoint: { max: 3000, min: 1024 },
            items: 4
        },
        tablet: {
            breakpoint: { max: 1024, min: 464 },
            items: 2
        },
        mobile: {
            breakpoint: { max: 464, min: 0 },
            items: 1
        }
    };

    useEffect(() => {
        getCities()
    }, [])

    return (
        <div className={classes.sections}>
            <div className={classes.container}>
                <Container maxWidth="lg">
                    <Grid container spacing={3}>
                        <Grid item xs={12} lg={12} md={12} sm={12}>
                            <h5 style={{ fontWeight: 'bold', fontSize: 22 }}>Why Quicktrip?</h5>
                        </Grid>
                        <Grid item xs={12} sm={12} style={{ textAlign: 'center' }}>
                            <Carousel
                                ssr={true} // means to render carousel on server-side.
                                draggable={false}
                                infinite={true}
                                autoPlay={true}
                                autoPlaySpeed={5000}
                                keyBoardControl={true}
                                customTransition="all 1"
                                transitionDuration={500}
                                containerClass="carousel-container"
                                responsive={responsive}
                                showDots
                                removeArrowOnDeviceType={["desktop", "tablet", "mobile"]}
                                dotListClass="custom-dot-list-style"
                                itemClass="carousel-item-padding-40-px">
                                <div style={{ height: 160 }}>
                                    <p className="text-center">
                                        <AvTimerIcon style={{ fontSize: 52, color: '#e53321' }}  />
                                    </p>
                                    <p className="text-center" style={{ marginBottom: 0 }}>
                                        Attention 24 hours
                                    </p>
                                    <p className="text-center" style={{ marginTop: 0 }}>
                                        7 days a week
                                    </p>
                                </div>
                                <div>
                                    <p className="text-center">
                                        <MonetizationOnIcon style={{ fontSize: 52, color: '#e53321' }}  />
                                    </p>
                                    <p className="text-center" style={{ marginBottom: 0 }}>
                                        Reduce costs with
                                    </p>
                                    <p className="text-center" style={{ marginTop: 0 }}>
                                        less travel fee
                                    </p>
                                </div>
                                <div>
                                    <p className="text-center">
                                        <RoomIcon style={{ fontSize: 52, color: '#e53321' }}  />
                                    </p>
                                    <p className="text-center" style={{ marginBottom: 0 }}>
                                        Locate Places
                                    </p>
                                    <p className="text-center" style={{ marginTop: 0 }}>
                                        tourist
                                    </p>
                                </div>
                                <div>
                                    <p className="text-center">
                                        <AccessTimeIcon style={{ fontSize: 52, color: '#e53321' }}  />
                                    </p>
                                    <p className="text-center" style={{ marginBottom: 0 }}>
                                        Find your guide
                                    </p>
                                    <p className="text-center" style={{ marginTop: 0 }}>
                                        in less time
                                    </p>
                                </div>
                                <div>
                                    <p className="text-center">
                                        <SecurityIcon style={{ fontSize: 52, color: '#e53321' }}  />
                                    </p>
                                    <p className="text-center" style={{ marginBottom: 0 }}>
                                        Security
                                    </p>
                                    <p className="text-center" style={{ marginTop: 0 }}>
                                        and warranty
                                    </p>
                                </div>
                                <div>
                                    <p className="text-center">
                                        <DateRangeIcon style={{ fontSize: 52, color: '#e53321' }}  />
                                    </p>
                                    <p className="text-center" style={{ marginBottom: 0 }}>
                                        Flexibility
                                    </p>
                                    <p className="text-center" style={{ marginTop: 0 }}>
                                        of schedules
                                    </p>
                                </div>
                            </Carousel>
                        </Grid>
                        <Grid item xs={12} lg={12} md={12} sm={12}>
                            <h5 style={{ fontWeight: 'bold', fontSize: 22 }}>Online Experiences</h5>
                        </Grid>
                        { cities && (
                            cities.map(city => (
                                <Grid item xs={12} lg={3} md={4} sm={3}>
                                    <Card className="card-content">
                                        <Link href={"/list-guide?city=" + city.name} style={{ textDecoration: "none", color: '#222222' }}>
                                            <CardActionArea>
                                                <CardMedia
                                                    component="img"
                                                    alt="Contemplative Reptile"
                                                    height="300"
                                                    image={city.image}
                                                    title="Contemplative Reptile"
                                                />
                                                <CardContent>
                                                    <p className="text-primary">{ city.name }</p>
                                                    <TextTruncate
                                                        line={5}
                                                        element="span"
                                                        truncateText="…"
                                                        text={ city.decription }
                                                        textTruncateChild={
                                                            (<a href={ '/list-guide?city=' + city.name }>Show more</a>)
                                                        }
                                                    />
                                                </CardContent>
                                            </CardActionArea>
                                        </Link>
                                    </Card>
                                </Grid>
                            ))
                        )}
                    </Grid>
                </Container>
            </div>
        </div>
    );
}
