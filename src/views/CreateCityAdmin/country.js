// Top 100 films as rated by IMDb users. http://www.imdb.com/chart/top
const countries = [
    { title: 'Perú', abrev: 'PER' },
    { title: 'Chile', abrev: 'CHL' },
    { title: 'Ecuador', abrev: 'ECU' },
    { title: 'Bolivia', abrev: 'BOL' },
    { title: 'Argentina', abrev: 'ARG' },
    { title: 'Brasil', abrev: 'BRA' },
    { title: 'Uruaguay', abrev: 'URU' },
];

export default countries