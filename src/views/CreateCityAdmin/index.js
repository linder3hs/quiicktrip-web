import React, {useState} from "react"
import SideMenu from '../SideMenu'
import Container from "@material-ui/core/Container"
import Grid from "@material-ui/core/Grid"
import {makeStyles} from '@material-ui/core/styles'
import Card from "../../components/Card/Card"
import CardContent from "@material-ui/core/CardContent"
import TextField from "@material-ui/core/TextField"
import Button from "@material-ui/core/Button"
import Autocomplete from '@material-ui/lab/Autocomplete'
// JSON
import countries from "./country";
// Firebase
import firebase, {storage} from '../../service/firebaseConfig'
import ClipLoader from "react-spinners/ClipLoader";
import '../CreateGuide/index.css'
import empty from 'is-empty'

const useStyles = makeStyles((theme) => ({
    toolbar: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end',
        padding: theme.spacing(0, 1),
        // necessary for content to be below app bar
        ...theme.mixins.toolbar,
    },
    content: {
        flexGrow: 1,
        padding: theme.spacing(3),
    },
    buttonExperience: {
        marginTop: 10,
        textAlign: 'right'
    }
}))

export default function HomeAdmin() {

    const classes = useStyles()
    // Variables
    const [name, setName] = useState("")
    const [country, setCountry] = useState("")
    const [description, setDescription] = useState("")
    const [image, setImage] = useState("")

    const [show, setShow] = useState(false)

    const handledName = (event) => setName(event.target.value)
    const handledCountry = (event, newInputValue) => setCountry(newInputValue)
    const handledDescription = (event) => setDescription(event.target.value)

    // Upload image to firebase
    const handleImageAsFile = (e) => {
        let reader = new FileReader()
        const image = e.target.files[0]
        if (image && image.type.match('image.*')) {
            reader.readAsDataURL(image)
        }
        setShow(true)
        setTimeout(() => {
            handleFireBaseUpload(image)
        }, 2000);
    }

    const handleFireBaseUpload = (image) => {
        //e.preventDefault()
        console.log('start of upload')
        // async magic goes here...
        if (image === '') {
            console.error(`not an image, the image file is a ${typeof (image)}`)
        }

        const uploadTask = storage.ref(`/images/${image.name}`).put(image)
        uploadTask.on('state_changed',
            (snapShot) => {
                //takes a snap shot of the process as it is happening
                console.log(snapShot)
            }, (err) => {
                //catches the errors
                console.log(err)
            }, () => {
                // gets the functions from storage refences the image storage in firebase by the children
                // gets the download url then sets the image from firebase as the value for the imgUrl key:
                storage.ref('images').child(image.name).getDownloadURL()
                    .then(fireBaseUrl => {
                        setImage(fireBaseUrl)
                        setShow(false)
                        console.log("image url")
                    })
            })
    }

    const createCity = () => {
        if (!empty(name) && !empty(country) && !empty(image) && !empty(description)) {
            const ref = firebase.database().ref("city")
            console.log(image)
            ref.push({
                'name': name,
                'country': country,
                'image': image,
                'decription': description
            })
            setTimeout(() => {
                window.location.replace("/list-city")
            }, 2000)
        }
    }

    const Example = () => (
        show ? (
            <div className="flex-container">
                <div className="row">
                    <div className="flex-item">
                        <ClipLoader
                            size={150}
                            color={"#123abc"}
                            loading={show}
                        />
                    </div>
                </div>
            </div>
        ) : ""
    );


    return (
        <div>
            <SideMenu/>
            <Container maxWidth="lg">
                <div style={{paddingTop: 40}}>
                    <Container maxWidth="lg" style={{paddingTop: 20}}>
                        <Grid container spacing={3}>
                            <Card>
                                <CardContent>
                                    <Grid container spacing={2}>
                                        <Grid item xs={12} sm={12}>
                                            <h2>Crear una Ciudad</h2>
                                        </Grid>
                                        <Grid item xs={12} sm={6} className={classes.buttonExperience}>
                                            <TextField onChange={handledName} fullWidth id="outlined-basic"
                                                       label="Nombre"
                                                       variant="outlined"/>
                                        </Grid>
                                        <Grid item xs={12} sm={6} className={classes.buttonExperience}>
                                            <Autocomplete
                                                id="combo-box-demo"
                                                options={countries}
                                                onInputChange={handledCountry}
                                                getOptionLabel={(option) => option.title}
                                                renderInput={(params) => <TextField {...params} label="País"
                                                                                    variant="outlined"/>}
                                            />
                                        </Grid>
                                        <Grid item xs={12} sm={6}>
                                            <TextField type="file" onChange={handleImageAsFile} fullWidth
                                                       id="outlined-basic"
                                                       variant="outlined"/>
                                            <Example/>
                                        </Grid>
                                        <Grid item xs={12} sm={12}>
                                            <TextField
                                                fullWidth
                                                onChange={handledDescription}
                                                id="outlined-basic"
                                                label="Descripcion"
                                                multiline
                                                rows={4}
                                                variant="outlined"/>
                                        </Grid>
                                        <Grid item xs={12} sm={12}>
                                            <Button onClick={() => createCity()} className="btn-primary" size="large">Crear
                                                Ciudad</Button>
                                        </Grid>
                                    </Grid>
                                </CardContent>
                            </Card>
                        </Grid>
                    </Container>
                </div>
            </Container>
        </div>
    )
}
