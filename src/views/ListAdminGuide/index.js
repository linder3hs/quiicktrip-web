import React, {useState, useEffect} from "react"
import {makeStyles} from "@material-ui/core/styles"
import firebase from "../../service/firebaseConfig"
// Header
import SideMenu from '../SideMenu'
import Container from "@material-ui/core/Container"
import Grid from "@material-ui/core/Grid"
import Button from "@material-ui/core/Button";
import {Link} from "react-router-dom";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardMedia from "@material-ui/core/CardMedia";
import CardContent from "@material-ui/core/CardContent";
import TextTruncate from "react-text-truncate";
import CardActions from "@material-ui/core/CardActions";
import BeautyStars from 'beauty-stars';

const useStyles = makeStyles({
    root: {
        maxWidth: 345,
    },
    media: {
        height: 140,
    },
});

export default function ListAdminGuide() {
    const classes = useStyles();
    const [listGuide, setListGuide] = useState([])

    useEffect(() => {
        getListGuide()
    }, [])

    const getListGuide = async () => {
        const ref = firebase.database().ref("guide")
        await ref.on('value', snap => {
            let requests = [];
            snap.forEach(function (data) {
                const info = {
                    "key": data.key,
                    "city": data.val().city,
                    "star": data.val().star,
                    "decription": data.val().decription,
                    "image": data.val().image,
                    "name": data.val().name,
                    "language": data.val().language,
                }
                requests.push(info);
            });
            setListGuide(requests)
        })
    }

    const deleteGuide = async (key) => {
        firebase.database().ref("guide/" + key).remove()
        getListGuide()
    }

    return (
        <div>
            <SideMenu/>
            <Container maxWidth="lg" style={{paddingTop: 60}}>
                <Grid container spacing={3}>
                    <Grid item xs={6} sm={6}>
                        <h2>List of Guides</h2>
                    </Grid>
                    <Grid item sm={6} xs={6} style={{textAlign: 'right', paddingTop: 38}}>
                        <Link to="/create-guide">
                            <Button className="btn-primary" size="large">Create Guide</Button>
                        </Link>
                    </Grid>
                    {listGuide && (
                        listGuide.map(guide => (
                            <Grid item xs={12} lg={3} md={3} sm={3}>
                                <Card className="card-content">
                                    <CardActionArea>
                                        <CardMedia
                                            component="img"
                                            alt="Contemplative Reptile"
                                            height="270"
                                            image={guide.image}
                                            title="Contemplative Reptile"
                                        />
                                        <CardContent>
                                            <p className="text-primary">{guide.name}</p>
                                            <TextTruncate
                                                line={7}
                                                element="span"
                                                truncateText="…"
                                                text={guide.decription}
                                            />
                                            <div className="top-20">
                                                <p className="text-primary">Languages</p>
                                                <p className="text-seconday">{guide.language}</p>
                                                <BeautyStars size={20} value={guide.star}/>
                                            </div>
                                        </CardContent>
                                        <CardActions>
                                            <Button onClick={ () => deleteGuide(guide.key)} size="small" color="primary">
                                                Eliminar
                                            </Button>
                                            <Link to={"/edit-guide?key=" + guide.key}>
                                                <Button size="small" color="primary">
                                                    Editar
                                                </Button>
                                            </Link>
                                        </CardActions>
                                    </CardActionArea>
                                </Card>
                            </Grid>
                        ))
                    )}
                </Grid>
            </Container>
        </div>
    )

}
