import React, { useState, useEffect } from "react"
import Container from "@material-ui/core/Container"
import Grid from "@material-ui/core/Grid"
// Header
import SideMenu from '../SideMenu'
// Card
import { makeStyles } from '@material-ui/core/styles'
import Card from '@material-ui/core/Card'
import CardActionArea from '@material-ui/core/CardActionArea'
import CardActions from '@material-ui/core/CardActions'
import CardContent from '@material-ui/core/CardContent'
import CardMedia from '@material-ui/core/CardMedia'
import Button from '@material-ui/core/Button'
import Typography from '@material-ui/core/Typography'
// Firebase
import firebase from "../../service/firebaseConfig"
import { Link } from 'react-router-dom'
import TextTruncate from "react-text-truncate"

const useStyles = makeStyles({
    root: {
        maxWidth: 345,
    },
    media: {
        height: 140,
    },
});

export default function ListAdminCity() {
    const classes = useStyles();

    const [listCity, setListCity] = useState([])

    useEffect(() => {
        getListCity()
    }, [])

    const getListCity = async () => {
        const ref = firebase.database().ref("city")
        await ref.on('value', snap => {
            let requests = [];
            snap.forEach(function (data) {
                const info = {
                    "key": data.key,
                    "country": data.val().country,
                    "decription": data.val().decription,
                    "image": data.val().image,
                    "name": data.val().name,
                }
                requests.push(info);
            });
            console.log(requests)
            setListCity(requests)
        })
    }

    const deleteCity = (key) => {
        firebase.database().ref("city/" + key).remove()
        getListCity()
    }

    return (
        <div>
            <SideMenu />
            <Container maxWidth="lg" style={{ paddingTop: 60 }}>
                <Grid container spacing={3}>
                    <Grid item sm={6} xs={6}>
                        <h2>List of City</h2>
                    </Grid>
                    <Grid item sm={6} xs={6} style={{ textAlign: 'right', paddingTop: 44 }}>
                        <Link to="/create-city">
                            <Button className="btn-primary" size="large">Create City</Button>
                        </Link>
                    </Grid>
                    { listCity && (
                        listCity.map(city => (
                            <Grid item xs={12} lg={3} md={3} sm={3}>
                                <Card className="card-content">
                                    <Link href={"/list-guide?city=" + city.name} style={{ textDecoration: "none", color: '#222222' }}>
                                        <CardActionArea>
                                            <CardMedia
                                                component="img"
                                                alt="Contemplative Reptile"
                                                height="300"
                                                image={city.image}
                                                title="Contemplative Reptile"
                                            />
                                            <CardContent>
                                                <p className="text-primary">{ city.name }</p>
                                                <TextTruncate
                                                    line={5}
                                                    element="span"
                                                    truncateText="…"
                                                    text={ city.decription }
                                                    textTruncateChild={
                                                        (<a href={ '/list-guide?city=' + city.name }>Show more</a>)
                                                    }
                                                />
                                            </CardContent>
                                        </CardActionArea>
                                        <CardActions>
                                            <Button onClick={ () => deleteCity(city.key) } size="small" color="primary">
                                                Eliminar
                                            </Button>
                                            <Link to={"/edit-city?key=" + city.key}>
                                                <Button size="small" color="primary">
                                                    Editar
                                                </Button>
                                            </Link>
                                        </CardActions>
                                    </Link>
                                </Card>
                            </Grid>
                        ))
                    )}
                </Grid>
            </Container>
        </div>
    )

}
