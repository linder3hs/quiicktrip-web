import React, {useEffect, useState} from "react"
import Container from "@material-ui/core/Container"
import Grid from "@material-ui/core/Grid"
import Card from '@material-ui/core/Card';
import CardMedia from "@material-ui/core/CardMedia"
import Button from "@material-ui/core/Button"
import {makeStyles} from "@material-ui/core/styles"
// Dialog
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogTitle from '@material-ui/core/DialogTitle'
import TextField from "@material-ui/core/TextField"
import './index.css'
import '../../index.css'
import '../../index.scss'
// Componente header
import Footer from "../../components/Footer/Footer"
import Avatar from "@material-ui/core/Avatar";
import Link from "@material-ui/core/Link";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardContent from "@material-ui/core/CardContent";
// Firebase
import firebase from "../../service/firebaseConfig";
import TextTruncate from "react-text-truncate";
import logo from "../../assets/img/logo.png";
import Header from "../../components/Header/Header.js";
import HeaderLinks from "../../components/Header/HeaderLinks";
import WhatsAppIcon from '@material-ui/icons/WhatsApp';
import MailIcon from '@material-ui/icons/Mail';
import IconButton from "@material-ui/core/IconButton";
// Country
import countries from "../../components/Header/extra/country";
import Autocomplete from "@material-ui/lab/Autocomplete";
import Select from '@material-ui/core/Select';
import InputLabel from "@material-ui/core/InputLabel";
import FormControl from "@material-ui/core/FormControl";
import MenuItem from "@material-ui/core/MenuItem";
// Mail
import SendMail from "../../components/SendMail/sendMail";
import classNames from "classnames";
import BeautyStars from "beauty-stars";
import styles from "../../assets/jss/material-kit-react/views/components";
const useStyles = makeStyles(styles);
export default function Detail(props) {
    const classes = useStyles();
    const {...rest} = props;
    const [open, setOpen] = useState(false)
    const [cities, setCities] = useState([])
    const [experiences, setExperiences] = useState([])
    // Guide
    const [name, setName] = useState("")
    const [photo, setPhoto] = useState("")
    const [guideDescription, setGuideDescription] = useState("")
    const [languages, setLanguages] = useState("")
    // Tour
    const [imageOne, setImageOne] = useState("")
    const [imageTwo, setImageTwo] = useState("")
    const [imageThree, setImageThree] = useState("")
    const [imageFour, setImageFour] = useState("")
    const [video, setVideo] = useState("")
    const [title, setTitle] = useState("")
    const [key, setKey] = useState("")
    const [description, setDescription] = useState("")
    const [itinerary, setItinerary] = useState("")
    const [connection, setConnection] = useState("")
    const [city, setCity] = useState("")

    // Mail options
    const [showMailForm, setShowMailForm] = useState(false)

    // Data to mail
    const [nameMail, setNameMail] = useState(localStorage.getItem("name") ?? "")
    const [countryMail, setCountryMail] = useState(localStorage.getItem("country") ?? "")
    const [phoneMail, setPhoneMail] = useState(localStorage.getItem("phone") ?? "")
    const [mailToMail, setMailToMail] = useState(localStorage.getItem("email") ?? "")

    // Select experience
    const [newExperience, setNewExperience] = useState('');

    const handleChange = (event) => {
        const keyExp = event.target.value
        experiences.map(experience => {
            if (experience.key === keyExp) {
                setNewExperience(experience.key);
                setImageOne(experience?.imageOne)
                setImageTwo(experience?.imageTwo)
                setImageThree(experience?.imageThree)
                setImageFour(experience?.imageFour)
                setVideo(experience?.video)
                setTitle(experience?.title)
                setCity(experience?.city)
                setKey(experience?.key)
                setDescription(experience?.description)
                setItinerary(experience?.itinerary)
                setConnection(experience?.connection)
            }
        })
    };

    const handledClickBookNow = () => {
        if (nameMail !== "" && mailToMail !== "" && phoneMail !== "" && countryMail !== "" && name !== "" && title) {
            const body = {
                "name": nameMail,
                "email": mailToMail,
                "phone": phoneMail,
                "country": countryMail,
                "guide": name,
                "title": title
            }
            SendMail.send(body)
            handleClose()
        } else {
            alert("Complete all fields.")
        }
    }

    const getCities = async () => {
        const ref = firebase.database().ref("city")
        await ref.on('value', snap => {
            let requests = [];
            snap.forEach(function (data) {
                const info = {
                    "key": data.key,
                    "country": data.val().country,
                    "decription": data.val().decription,
                    "image": data.val().image,
                    "name": data.val().name,
                }
                requests.push(info);
            });
            setCities(requests)
        })
    }

    function countryToFlag(isoCode) {
        return typeof String.fromCodePoint !== 'undefined'
            ? isoCode
                .toUpperCase()
                .replace(/./g, (char) => String.fromCodePoint(char.charCodeAt(0) + 127397))
            : isoCode;
    }

    const getExperiencesByGuide = async () => {
        const nameFromRoute = (new URLSearchParams(window.location.search)).get("guide")
        const ref = firebase.database().ref("experience")
        await ref.on('value', snap => {
            let experiences = []
            snap.forEach(function (data) {
                if (data.val()?.guide === nameFromRoute) {
                    const info = {
                        "key": data.key,
                        "city": data.val()?.city,
                        "description": data.val()?.description,
                        "itinerary": data.val()?.itinerary,
                        "guide": data.val()?.guide,
                        "title": data.val()?.title,
                        "connection": data.val()?.connection,
                        "imageOne": data.val()?.imageOne,
                        "imageTwo": data.val()?.imageTwo,
                        "imageThree": data.val()?.imageThree,
                        "imageFour": data.val()?.imageFour,
                        "video": data.val()?.video
                    }
                    experiences.push(info)
                }
            })
            // Set data defaul
            setImageOne(experiences[0]?.imageOne)
            setImageTwo(experiences[0]?.imageTwo)
            setImageThree(experiences[0]?.imageThree)
            setImageFour(experiences[0]?.imageFour)
            setVideo(experiences[0]?.video)
            setTitle(experiences[0]?.title)
            setCity(experiences[0]?.city)
            setKey(experiences[0]?.key)
            setDescription(experiences[0]?.description)
            setItinerary(experiences[0]?.itinerary)
            setConnection(experiences[0]?.connection)
            setExperiences(experiences)
            setNewExperience(experiences[0]?.key)
        })
    }

    const getGuidesByName = async () => {
        const ref = firebase.database().ref("guide")
        let nameFromRoute = (new URLSearchParams(window.location.search)).get("guide")
        nameFromRoute = nameFromRoute.split("-")
        await ref.on('value', snap => {
            let requests = [];
            snap.forEach(function (data) {
                if (data.val().name === nameFromRoute[0]) {
                    const info = {
                        "key": data.key,
                        "city": data.val().city,
                        "decription": data.val().decription,
                        "image": data.val().image,
                        "name": data.val().name,
                        "language": data.val().language,
                    }
                    requests.push(info);
                }

            });
            setName(requests[0]?.name)
            setPhoto(requests[0]?.image)
            setGuideDescription(requests[0]?.decription)
            setLanguages(requests[0]?.language)
        })
    }

    useEffect(() => {
        getCities()
        getGuidesByName()
        getExperiencesByGuide()
    }, [])

    const handleClickOpen = () => {
        setOpen(true);
    }

    const handleClose = () => {
        setOpen(false);
    }

    return (
        <div>
            <Header
                brand={(
                    <a href="/">
                        <img src={logo} height={50}/>
                    </a>
                )}
                rightLinks={<HeaderLinks/>}
                fixed
                color="white"
                changeColorOnScroll={{
                    height: 400,
                    color: "white"
                }}
                {...rest}
            />
            <br/><br/>
            <div className="photos-bg">
                <Container maxWidth="lg">
                    <br/><br/>
                    <Grid container spacing={2}>
                        <Grid item sm={3} xs={6}>
                            <Grid container>
                                <Grid xs={12} sm={12}
                                      style={{backgroundColor: 'black', borderRadius: 8, textAlign: 'center'}}>
                                    {
                                        video && (
                                            <video src={video} autoPlay loop controls muted className="video-content"/>
                                        )
                                    }
                                </Grid>
                                <Grid xs={12} sm={12} className="card-top">
                                    {
                                        imageTwo && (
                                            <Card>
                                                <CardMedia
                                                    component="img"
                                                    alt="image"
                                                    height="205"
                                                    image={imageTwo}
                                                    title="Contemplative Reptile"
                                                />
                                            </Card>
                                        )
                                    }
                                </Grid>
                            </Grid>
                        </Grid>
                        <Grid item sm={3} xs={6} className="card-top">
                            <Card>
                                <CardMedia
                                    component="img"
                                    alt="image"
                                    height="420"
                                    image={imageThree}
                                    title="Contemplative Reptile"
                                />
                            </Card>
                        </Grid>
                        <Grid item sm={3} xs={6} className="card-top">
                            <Card>
                                <CardMedia
                                    component="img"
                                    alt="image"
                                    height="420"
                                    image={imageFour}
                                    title="Contemplative Reptile"
                                />
                            </Card>
                        </Grid>
                        <Grid item sm={3} xs={6} className="card-top">
                            <Card>
                                <CardMedia
                                    component="img"
                                    alt="image"
                                    height="420"
                                    image={imageOne}
                                    title="Contemplative Reptile"
                                />
                            </Card>
                        </Grid>
                        <Grid item sm={3} xs={6}>
                            {experiences && (
                                <FormControl variant="outlined" className="select-content">
                                    <InputLabel id="demo-simple-select-outlined-label">Experience</InputLabel>
                                    <Select
                                        labelId="demo-simple-select-outlined-label"
                                        id="demo-simple-select-outlined"
                                        value={newExperience}
                                        onChange={handleChange}
                                        label="Experience">
                                        {experiences.map(experience => (
                                            <MenuItem value={experience.key}>{experience.title}</MenuItem>
                                        ))}
                                    </Select>
                                </FormControl>
                            )}
                            <h5 className="text-black">{city}</h5>
                            {photo && (
                                <Avatar alt="Guide Image"
                                        src={photo}/>
                            )}
                            {name && (<h4 className="text-black">{name}</h4>)}
                        </Grid>
                        <Grid item sm={9} xs={6}>
                            <br/>
                            <p className="text-black text-middle">{connection}</p>
                            <br/>
                            <Button onClick={handleClickOpen} size="large" className="btn-primary">Book Now</Button>
                            <Dialog
                                open={open}
                                onClose={handleClose}
                                aria-labelledby="alert-dialog-title"
                                aria-describedby="alert-dialog-description"
                            >
                                <DialogTitle id="alert-dialog-title" style={{textAlign: 'center'}}><span>Reserve your experience</span></DialogTitle>
                                <DialogContent className="padding-34">
                                    <DialogContentText id="alert-dialog-description">
                                        <Grid container direction={"row"} spacing={3}>
                                            <Grid item sm={12} xs={12} className="text-center">
                                                <p className="myText top-20">
                                                    Choose the method by which you want me to contact you.
                                                </p>
                                            </Grid>
                                            <Grid item sm={6} xs={12} className="text-center">
                                                <IconButton
                                                    target="_blank"
                                                    style={{color: '#e53321'}}
                                                    href={"https://api.whatsapp.com/send?phone=+51945917878&text=" + "Hello, I am interested in the Tour " + title + " with the guide " + name + "&source=&data=&app_absent="}>
                                                    <WhatsAppIcon fontSize="large"/>
                                                </IconButton>
                                            </Grid>
                                            <Grid item sm={6} xs={12} className="text-center">
                                                <IconButton onClick={() => setShowMailForm(true)} aria-label="Mail">
                                                    <MailIcon style={{color: '#e53321'}} fontSize="large"/>
                                                </IconButton>
                                            </Grid>
                                            {
                                                showMailForm ? (
                                                    <React.Fragment>
                                                        <Grid item sm={12} xs={12}>
                                                            <TextField
                                                                defaultValue={nameMail} fullWidth
                                                                onChange={(e) => setNameMail(e.target.value)}
                                                                label="Full Name" variant="outlined"/>
                                                        </Grid>
                                                        <Grid item sm={12} xs={12}>
                                                            <TextField defaultValue={phoneMail} fullWidth
                                                                       onChange={(e) => setPhoneMail(e.target.value)}
                                                                       label="Phonenumber" variant="outlined"/>
                                                        </Grid>
                                                        <Grid item sm={12} xs={12}>
                                                            <Autocomplete
                                                                id="country-select-demo"
                                                                options={countries}
                                                                defaultValue={countries.find(v => v.label + " " + v.phone === countryMail)}
                                                                onChange={(event, newValue) => {
                                                                    setCountryMail(newValue.label + " " + newValue.phone)
                                                                }}
                                                                autoHighlight
                                                                getOptionLabel={(option) => option.label + " +" + option.phone}
                                                                renderOption={(option) => (
                                                                    <React.Fragment>
                                                                        <span>{countryToFlag(option.code)}</span>
                                                                        {option.label} ({option.code}) +{option.phone}
                                                                    </React.Fragment>
                                                                )}
                                                                renderInput={(params) => (
                                                                    <TextField
                                                                        {...params}
                                                                        label="Choose a country"
                                                                        defaultValue={countryMail}
                                                                        variant="outlined"
                                                                        inputProps={{
                                                                            ...params.inputProps,
                                                                            autoComplete: 'new-password', // disable autocomplete and autofill
                                                                        }}
                                                                    />
                                                                )}
                                                            />
                                                        </Grid>
                                                        <Grid item sm={12} xs={12}>
                                                            <TextField defaultValue={mailToMail} fullWidth label="Email"
                                                                       onChange={(e) => setMailToMail(e.target.value)}
                                                                       variant="outlined"/>
                                                        </Grid>
                                                        <Grid item sm={12} xs={12}>
                                                            <Button onClick={() => handledClickBookNow()}
                                                                    className="btn-primary" size="large">Book
                                                                Now</Button>
                                                        </Grid>
                                                    </React.Fragment>
                                                ) : ""
                                            }
                                        </Grid>
                                    </DialogContentText>
                                </DialogContent>
                            </Dialog>

                        </Grid>
                    </Grid>
                </Container>
            </div>
            <div className={classNames(classes.main, classes.mainRaised)}>
                <div className={classes.sections}>
                    <div className={classes.container}>
                        <div className={classes.title}>
                            {/*  Aqui va la parte blanca */}
                            <div className="container-text" style={{ fontWeight: "normal" }}>
                                <Container maxWidth="lg">
                                    <Grid container spacing={3}>
                                        <Grid item xs={12} sm={4}>
                                            <h2 className="title-description">Tour Description</h2>
                                        </Grid>
                                        <Grid item xs={12} sm={8}>
                                            <p className="text-description">{description}</p>
                                        </Grid>
                                        <Grid item xs={12} sm={4}>
                                            <h2 className="title-description">Itinerary</h2>
                                        </Grid>
                                        <Grid item xs={12} sm={8}>
                                            <p className="text-description">{itinerary}</p>
                                        </Grid>
                                        <Grid item sm={3} xs={12} className="grip-top">
                                            <h2 className="title-description">An Innovative Experience</h2>
                                        </Grid>
                                        <Grid item sm={3} xs={12} className="grip-top">
                                            <p>
                                                <img width={32} height={32}
                                                     src="https://a0.muscache.com/pictures/2f1e240c-d383-45e4-b34b-8957d061cb32.jpg"
                                                     alt=""/>
                                            </p>
                                            <p><strong>Skilled Hosts</strong></p>
                                            <p>
                                                Quicktrip guides are ready to share their knowledge on these wonderful
                                                places around Peru.
                                            </p>
                                        </Grid>
                                        <Grid item sm={3} xs={12} className="grip-top">
                                            <p>
                                                <img width={32} height={32}
                                                     src="https://a0.muscache.com/pictures/6ca44422-9ab4-42d0-94a1-73fff922164e.jpg"
                                                     alt=""/>
                                            </p>
                                            <p><strong>Group or Individual Activities:</strong></p>
                                            <p>
                                                The virtual activities are done in small groups so you have the chance to talk to people
                                                from all over the world as you tour new places. Private tours can also be arranged.
                                            </p>
                                        </Grid>
                                        <Grid item sm={3} xs={12} className="grip-top">
                                            <p>
                                                <img width={32} height={32}
                                                     src="https://a0.muscache.com/pictures/1ae9646f-4fdc-487a-a6f0-fea0a4a6cc44.jpg"
                                                     alt=""/>
                                            </p>
                                            <p><strong>Guides considered</strong></p>
                                            <p>
                                                Meet the guides who share their expertise and open a window to your world.
                                            </p>
                                        </Grid>
                                        <Grid item xs={12} sm={4}>
                                            <h2 className="title-description">Meet Your Guide</h2>
                                        </Grid>
                                        <Grid item xs={12} sm={8}>
                                            {photo && (<Avatar className="img-large" alt="Linder Hassinger" src={photo}/>)}
                                            {name && (<h2>{name}</h2>)}
                                            {guideDescription && (<p className="text-description">{guideDescription}</p>)}
                                            <p>
                                                <span className="text-primary">Languages</span><br/>
                                                {languages && (<span className="text-seconday">{languages}</span>)}
                                            </p>
                                        </Grid>
                                        {/*<Grid item xs={12} sm={4}>*/}
                                        {/*    <h2 className="title-description">Cómo participar</h2>*/}
                                        {/*</Grid>*/}
                                        {/*<Grid item xs={12} sm={8}>*/}
                                        {/*    <h3>Únete a una videollamada</h3>*/}
                                        {/*    <p className="text-description">*/}
                                        {/*        <a target="_blank" href="https://zoom.us/download">Descarga Zoom</a> de forma gratuita*/}
                                        {/*        en una computadora o en un dispositivo móvil. Una vez*/}
                                        {/*        que hayas reservado, recibirás un correo electrónico con un enlace y la información*/}
                                        {/*        sobre cómo unirte.*/}
                                        {/*    </p>*/}
                                        {/*</Grid>*/}
                                        <Grid item xs={12} sm={12}>
                                            <h2 className="title-description">More online experiences</h2>
                                        </Grid>
                                        <Grid className="content-more" container spacing={3}>
                                            {cities && (
                                                cities.map(city => (
                                                    <Grid item xs={12} lg={3} md={4} sm={3}>
                                                        <Card className="card-content">
                                                            <Link href={"/list-guide?city=" + city.name}
                                                                  style={{textDecoration: "none", color: '#222222'}}>
                                                                <CardActionArea>
                                                                    <CardMedia
                                                                        component="img"
                                                                        alt="Contemplative Reptile"
                                                                        height="300"
                                                                        image={city.image}
                                                                        title="Contemplative Reptile"
                                                                    />
                                                                    <CardContent>
                                                                        <p className="text-primary">{city.name}</p>
                                                                        <TextTruncate
                                                                            line={5}
                                                                            element="span"
                                                                            truncateText="…"
                                                                            text={city.decription}
                                                                            textTruncateChild={
                                                                                (<a href={'/list-guide?city=' + city.name}>Show more</a>)
                                                                            }
                                                                        />
                                                                    </CardContent>
                                                                </CardActionArea>

                                                            </Link>
                                                        </Card>
                                                    </Grid>
                                                ))
                                            )}
                                        </Grid>
                                    </Grid>
                                </Container>
                                <br/><br/>
                            </div>
                        </div>
                        <br/><br/>
                    </div>
                </div>
            </div>
            <Footer/>






            {/*<Footer/>*/}
        </div>
    )
}
