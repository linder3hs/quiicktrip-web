import React, {useEffect, useState} from "react"
import SideMenu from '../SideMenu'
import Container from "@material-ui/core/Container"
import Grid from "@material-ui/core/Grid"
import {makeStyles, useTheme} from '@material-ui/core/styles';
import Card from "../../components/Card/Card"
import CardContent from "@material-ui/core/CardContent"
import TextField from "@material-ui/core/TextField"
import Button from "@material-ui/core/Button"
// Firebase
import firebase, {storage} from "../../service/firebaseConfig";
import ClipLoader from "react-spinners/ClipLoader"
import '../CreateGuide/index.css'
import Autocomplete from "@material-ui/lab/Autocomplete"
import empty from "is-empty"
import countries from "../CreateCityAdmin/country";

const useStyles = makeStyles((theme) => ({
    toolbar: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end',
        padding: theme.spacing(0, 1),
        // necessary for content to be below app bar
        ...theme.mixins.toolbar,
    },
    content: {
        flexGrow: 1,
        padding: theme.spacing(3),
    },
}))

export default function HomeAdmin() {
    const classes = useStyles()
    const [show, setShow] = useState(false)

    // Variables
    const [title, setTitle] = useState("")
    const [city, setCity] = useState("")
    const [description, setDescription] = useState("")
    const [itinerary, setItinerary] = useState("")
    const [guide, setGuide] = useState("")
    const [connection, setConnection] = useState("")
    const [imageOne, setImageOne] = useState("")
    const [imageTwo, setImageTwo] = useState("")
    const [imageThree, setImageThree] = useState("")
    const [imageFour, setImageFour] = useState("")
    const [video, setVideo] = useState("")
    // get list
    const [listGuides, setListGuides] = useState([])

    const handledTitle = event => setTitle(event.target.value)
    const handledCity = event => setCity(event.target.value)
    const handledDescription = event => setDescription(event.target.value)
    const handledItinerary = event => setItinerary(event.target.value)
    const handledConnection = event => setConnection(event.target.value)
    const handledGuide = (event, newValue) => setGuide(newValue)

    // Upload image to firebase
    const handleImageAsFile = (e) => {
        let reader = new FileReader()
        const image = e.target.files[0]
        if (image && image.type.match('image.*')) {
            reader.readAsDataURL(image)
        }
        setShow(true)
        setTimeout(() => {
            handleFireBaseUpload(image)
        }, 2000);
    }

    const handleFireBaseUpload = (image) => {
        //e.preventDefault()
        console.log('start of upload')
        // async magic goes here...
        if (image === '') {
            console.error(`not an image, the image file is a ${typeof (image)}`)
        }

        const uploadTask = storage.ref(`/images/${image.name}`).put(image)
        uploadTask.on('state_changed',
            (snapShot) => {
                //takes a snap shot of the process as it is happening
                console.log(snapShot)
            }, (err) => {
                //catches the errors
                console.log(err)
            }, () => {
                // gets the functions from storage refences the image storage in firebase by the children
                // gets the download url then sets the image from firebase as the value for the imgUrl key:
                storage.ref('images').child(image.name).getDownloadURL()
                    .then(fireBaseUrl => {
                        setImageOne(fireBaseUrl)
                        console.log("image url")
                    })
                setShow(false)
            })
    }

    // Image two
    const handleImageAsFileTwo = (e) => {
        let reader = new FileReader()
        const image = e.target.files[0]
        setShow(true)
        if (image && image.type.match('image.*')) {
            reader.readAsDataURL(image)
        }
        setTimeout(() => {
            handleFireBaseUploadTwo(image)
        }, 2000);
    }

    const handleFireBaseUploadTwo = (image) => {
        //e.preventDefault()
        console.log('start of upload')
        // async magic goes here...
        if (image === '') {
            console.error(`not an image, the image file is a ${typeof (image)}`)
        }
        const uploadTask = storage.ref(`/images/${image.name}`).put(image)
        uploadTask.on('state_changed',
            (snapShot) => {
            }, (err) => {
                console.log(err)
            }, () => {
                storage.ref('images').child(image.name).getDownloadURL()
                    .then(fireBaseUrl => {
                        console.log(fireBaseUrl)
                        setImageTwo(fireBaseUrl)
                    })
                setShow(false)
            })
    }

    // Three image
    const handleImageAsFileThree = (e) => {
        let reader = new FileReader()
        const image = e.target.files[0]
        setShow(true)
        if (image && image.type.match('image.*')) {
            reader.readAsDataURL(image)
        }
        setTimeout(() => {
            handleFireBaseUploadThree(image)
        }, 2000);
    }

    const handleFireBaseUploadThree = (image) => {
        //e.preventDefault()
        console.log('start of upload')
        // async magic goes here...
        if (image === '') {
            console.error(`not an image, the image file is a ${typeof (image)}`)
        }
        const uploadTask = storage.ref(`/images/${image.name}`).put(image)
        uploadTask.on('state_changed',
            (snapShot) => {
            }, (err) => {
                console.log(err)
            }, () => {
                storage.ref('images').child(image.name).getDownloadURL()
                    .then(fireBaseUrl => {
                        console.log(fireBaseUrl)
                        setImageThree(fireBaseUrl)
                    })
                setShow(false)
            })
    }

    // Four image
    const handleImageAsFileFour = (e) => {
        let reader = new FileReader()
        const image = e.target.files[0]
        setShow(true)
        if (image && image.type.match('image.*')) {
            reader.readAsDataURL(image)
        }
        setTimeout(() => {
            handleFireBaseUploadFour(image)
        }, 2000);
    }

    const handleFireBaseUploadFour = (image) => {
        //e.preventDefault()
        console.log('start of upload')
        // async magic goes here...
        if (image === '') {
            console.error(`not an image, the image file is a ${typeof (image)}`)
        }
        const uploadTask = storage.ref(`/images/${image.name}`).put(image)
        uploadTask.on('state_changed',
            (snapShot) => {
            }, (err) => {
                console.log(err)
            }, () => {
                storage.ref('images').child(image.name).getDownloadURL()
                    .then(fireBaseUrl => {
                        console.log(fireBaseUrl)
                        setImageFour(fireBaseUrl)
                    })
                setShow(false)
            })
    }

    // Firebase Video
    const handleImageAsFileVideo = (e) => {
        let reader = new FileReader()
        const image = e.target.files[0]
        reader.readAsDataURL(image)
        setShow(true)
        setTimeout(() => {
            handleFireBaseUploadVideo(image)
        }, 2000);
    }

    const handleFireBaseUploadVideo = (image) => {
        //e.preventDefault()
        console.log('start of upload')
        // async magic goes here...
        if (image === '') {
            console.error(`not an image, the image file is a ${typeof (image)}`)
        }
        const uploadTask = storage.ref(`/images/${image.name}`).put(image)
        uploadTask.on('state_changed',
            (snapShot) => {
            }, (err) => {
                console.log(err)
            }, () => {
                storage.ref('images').child(image.name).getDownloadURL()
                    .then(fireBaseUrl => {
                        console.log(fireBaseUrl)
                        setVideo(fireBaseUrl)
                    })
                setShow(false)
            })
    }

    // Firebase get guides
    const getListGuides = async () => {
        const ref = firebase.database().ref("guide")
        await ref.on('value', snap => {
            let requests = [];
            snap.forEach(function (data) {
                const info = {
                    "name": data.val().name + "-" + data.val().city,
                }
                requests.push(info);
            });
            setListGuides(requests)
        })
    }

    const fetchUpdateExperience = async () => {
        if (!empty(title) && !empty(city) && !empty(guide) && !empty(description)) {
            const urlParams = new URLSearchParams(window.location.search)
            const myParam = urlParams.get('key')
            const ref = firebase.database().ref("experience").child(myParam)
            await ref.update({
                'title': title,
                'city': city,
                'guide': guide,
                'connection': connection,
                'description': description,
                'itinerary': itinerary,
                'imageOne': imageOne,
                'imageTwo': imageTwo,
                'imageThree': imageThree,
                'imageFour': imageFour,
                'video': video,
            })
            setTimeout(() => {
                window.location.replace("/list-experience")
            }, 2000)
        } else {
            alert("Completa todos los datos")
        }
    }

    const Example = ({type, color}) => (
        show ? (
            <div className="flex-container">
                <div className="row">
                    <div className="flex-item">
                        <ClipLoader
                            size={150}
                            color={"#123abc"}
                            loading={show}
                        />
                    </div>
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                </div>
            </div>
        ) : ""
    )

    const getExperience = async () => {
        const urlParams = new URLSearchParams(window.location.search)
        const myParam = urlParams.get('key')
        const ref = firebase.database().ref("experience").child(myParam)
        await ref.on('value', snap => {
            setCity(snap.val().city)
            setConnection(snap.val()?.connection)
            setDescription(snap.val()?.description)
            setItinerary(snap.val()?.itinerary)
            setGuide(snap.val().guide)
            setImageFour(snap.val()?.imageFour)
            setImageOne(snap.val()?.imageOne)
            setImageThree(snap.val()?.imageThree)
            setImageTwo(snap.val()?.imageTwo)
            setTitle(snap.val()?.title)
            setVideo(snap.val()?.video)
        })
    }

    useEffect(() => {
        getListGuides()
        getExperience()
    }, [])

    return (
        <div>
            <SideMenu/>
            <Container maxWidth="lg">
                <div style={{paddingTop: 40}}>
                    <Container maxWidth="lg" style={{paddingTop: 20}}>
                        <Grid container spacing={3}>
                            <Card>
                                <CardContent>
                                    <Grid container spacing={2}>
                                        <Grid item xs={12} sm={8}>
                                            <h2>Editar una experiencia</h2>
                                        </Grid>
                                        <Grid item xs={12} sm={4}>
                                            <Button href={'/list-experience'} className="btn-primary" size="large">Ver Experiencias</Button>
                                        </Grid>
                                        <Grid item xs={12} sm={6}>
                                            <TextField
                                                onChange={handledTitle}
                                                fullWidth
                                                value={title}
                                                id="outlined-basic"
                                                label="Titulo"
                                                variant="outlined"/>
                                        </Grid>
                                        <Grid item xs={12} sm={6}>
                                            <TextField
                                                onChange={handledCity}
                                                value={city}
                                                fullWidth id="outlined-basic"
                                                label="Ciudad de Experiencia"
                                                variant="outlined"/>
                                        </Grid>
                                        <Grid item xs={12} sm={12}>
                                            <TextField
                                                fullWidth
                                                id="outlined-basic"
                                                value={description}
                                                onChange={handledDescription}
                                                label="Descripcion"
                                                multiline
                                                rows={4}
                                                variant="outlined"/>
                                        </Grid>
                                        <Grid item xs={12} sm={12}>
                                            <TextField
                                                fullWidth
                                                id="outlined-basic"
                                                value={itinerary}
                                                onChange={handledItinerary}
                                                label="Itinerario"
                                                multiline
                                                rows={4}
                                                variant="outlined"/>
                                        </Grid>

                                        {
                                            guide && (
                                                <Grid item xs={12} sm={6}>
                                                    {listGuides && (
                                                        <Autocomplete
                                                            id="combo-box-demo"
                                                            options={listGuides}
                                                            defaultValue={listGuides.find(v => v.name === guide)}
                                                            onInputChange={handledGuide}
                                                            getOptionLabel={(option) => option.name}
                                                            renderInput={(params) => <TextField {...params}
                                                                                                label="Guias"
                                                                                                variant="outlined"/>}
                                                        />
                                                    )}
                                                </Grid>

                                            )
                                        }
                                        <Grid item xs={12} sm={6}>
                                            <TextField
                                                onChange={handledConnection}
                                                fullWidth
                                                value={connection}
                                                id="outlined-basic"
                                                label="Conexion"
                                                variant="outlined"/>
                                        </Grid>
                                        <Grid item xs={12} sm={12}>
                                            <h4>Cargar Fotos</h4>
                                            <Example/>
                                        </Grid>
                                        <Grid item xs={12} sm={6}>
                                            <TextField type="file" onChange={handleImageAsFile} fullWidth
                                                       id="outlined-basic" variant="outlined"/>
                                        </Grid>
                                        <Grid item xs={12} sm={6}>
                                            <TextField type="file" onChange={handleImageAsFileTwo} fullWidth
                                                       id="outlined-basic" variant="outlined"/>
                                        </Grid>
                                        <Grid item xs={12} sm={6}>
                                            <TextField type="file" onChange={handleImageAsFileThree} fullWidth
                                                       id="outlined-basic" variant="outlined"/>
                                        </Grid>
                                        <Grid item xs={12} sm={6}>
                                            <TextField type="file" onChange={handleImageAsFileFour} fullWidth
                                                       id="outlined-basic" variant="outlined"/>
                                        </Grid>
                                        <Grid item xs={12} sm={6}>
                                            <h4>Cargar Video</h4>
                                            <TextField type="file" onChange={handleImageAsFileVideo} fullWidth
                                                       id="outlined-basic" variant="outlined"/>
                                        </Grid>
                                        <Grid item xs={12} sm={12}>
                                            <Button onClick={() => fetchUpdateExperience()} className="btn-primary"
                                                    size="large">Actualizar Experiencia</Button>
                                        </Grid>
                                    </Grid>
                                </CardContent>
                            </Card>
                        </Grid>
                    </Container>
                </div>
            </Container>
        </div>
    )
}
