/*eslint-disable*/
import React, { useState } from "react";
// @material-ui/core components
import {makeStyles} from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import Tooltip from "@material-ui/core/Tooltip";
// Dialog
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import './index.css'
import Button from "@material-ui/core/Button"

import styles from "../../assets/jss/material-kit-react/components/headerLinksStyle.js"
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import Autocomplete from '@material-ui/lab/Autocomplete';
import '../../index.css'
import countries from './extra/country'
// Firebase
import firebase, { auth } from '../../service/firebaseConfig'

const useStyles = makeStyles(styles);

function countryToFlag(isoCode) {
    return typeof String.fromCodePoint !== 'undefined'
        ? isoCode
            .toUpperCase()
            .replace(/./g, (char) => String.fromCodePoint(char.charCodeAt(0) + 127397))
        : isoCode;
}

export default function HeaderLinks(props) {
    const classes = useStyles()
    const [open, setOpen] = useState(false)
    const [openRegister, setOpenRegister] = useState(false)

    // SigIn
    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")

    // SignUp
    const [name, setName] = useState("")
    const [emailRegister, setEmailRegister] = useState("")
    const [passwordRegister, setPasswordRegister] = useState("")
    const [country, setCountry] = useState("")
    const [phone, setPhone] = useState("")


    // Sign In
    const handledEmail = event => setEmail(event.target.value)
    const handledPassword = event => setPassword(event.target.value)

    // Sign Up
    const handledName = event => setName(event.target.value)
    const handledEmailRegister = event => setEmailRegister(event.target.value)
    const handledPasswordRegister = event => setPasswordRegister(event.target.value)
    const handledPhone = event => setPhone(event.target.value)

    const handleClickOpen = () => setOpen(true);
    const handleClose = () => setOpen(false);

    const handleClickOpenRegister = () => setOpenRegister(true);
    const handleCloseRegister = () => setOpenRegister(false);

    const fetchLogin = async () => {
        try {
            await auth.signInWithEmailAndPassword(email, password)
            let countryPhone = auth.currentUser?.photoURL
            console.log(countryPhone)
            countryPhone = countryPhone.split("-")
            console.log(countryPhone[0])
            console.log(countryPhone[1])
            const flag = countryPhone[0]
            const cell = countryPhone[1]
            await saveInfoSession(auth.currentUser?.displayName, auth.currentUser?.email, flag, cell)
        } catch (e) {
            console.log(e.toString())
        }
    }

    const fetchRegister = async () => {
        try {
            await auth.createUserWithEmailAndPassword(emailRegister, passwordRegister)
            await auth.currentUser.updateProfile({
                displayName: name,
                photoURL: country.label + " " + country.phone + "-" + phone
            })
            await addQuote()
        } catch (e) {
            console.log(e.toString())
        }
    }

    const addQuote = async () => {
        if (!auth.currentUser) {
            return alert("Not login")
        }
        const ref = firebase.database()
            .ref("users")
            .child(auth.currentUser.uid)
            .push({
                'name': name,
                'email': emailRegister,
                'phone': phone,
                'country': country
        })
        let countryPhone = auth.currentUser?.photoURL
        countryPhone = countryPhone.split("-")
        const flag = countryPhone[0]
        const cell = countryPhone[1]
        await saveInfoSession(auth.currentUser?.displayName, auth.currentUser?.email, flag, cell)
    }

    const saveInfoSession = async (name, email, country, phone) => {
        localStorage.setItem("auth", "login")
        localStorage.setItem("name", name)
        localStorage.setItem("email", email)
        localStorage.setItem("country", country)
        localStorage.setItem("phone", phone)
        window.location.reload()
    }

    const handleClickSignOut = async () => {
        await auth.signOut()
        localStorage.setItem("auth", "logout")
        localStorage.removeItem("name")
        localStorage.removeItem("email")
        localStorage.removeItem("country")
        localStorage.removeItem("phone")
        window.location.reload()
    }

    return (
        <List className={classes.list}>
            <ListItem className={classes.listItem}>
                <Button
                    color="white"
                    href={'/about'}
                    className={classes.navLink}>
                    About / FAQ
                </Button>

            </ListItem>
            <ListItem className={classes.listItem}>
                {
                    localStorage.getItem("auth") === "login" ? (
                        <Button
                            color="white"
                            className={classes.navLink}>
                            Hi!, { localStorage.getItem("name") }
                        </Button>
                    ) : (
                        <Button
                            onClick={handleClickOpen}
                            color="white"
                            className={classes.navLink}>
                            Sign In
                        </Button>
                    )
                }
                <Dialog
                    open={open}
                    onClose={handleClose}
                    aria-labelledby="alert-dialog-title"
                    aria-describedby="alert-dialog-description"
                    style={{ borderRadius: 12 }}
                >
                    <DialogTitle id="alert-dialog-title" style={{ fontSize:16, fontFamily:'Montserrat-Medium', textAlign: 'center', borderBottom: '1px solid rgba(34, 34, 34, 0.6)' }}>
                        <span style={{ fontFamily:'Montserrat-Medium'}}>Sign In</span>
                    </DialogTitle>
                    <DialogContent>
                        <DialogContentText id="alert-dialog-description">
                            <Grid container direction={"row"} spacing={5}>
                                <Grid item sm={12} xs={12}>
                                    <Grid item style={{ marginTop: 20 }}>
                                        <TextField onChange={handledEmail} className="input-login" fullWidth label="E-mail" variant="outlined" />
                                    </Grid>
                                    <Grid item>
                                        <TextField onChange={handledPassword} type="password" className="input-login" fullWidth label="Password" variant="outlined" />
                                        <span style={{ fontSize: 12 }}>
                                            Start to know the best experiences in Peru from the comfort of your home.
                                        </span>
                                    </Grid>
                                    <br/>
                                    <Grid item style={{ textAlign:'right' }}>
                                        <Button onClick={() => fetchLogin()} fullWidth variant="contained" color="primary">
                                            Keep going
                                        </Button>
                                    </Grid>
                                    <br/>
                                    {/*<Grid item style={{ textAlign:'center' }}>*/}
                                    {/*    <Button fullWidth variant="outlined" color="secondary">*/}
                                    {/*        Continúa con Google*/}
                                    {/*    </Button>*/}
                                    {/*</Grid>*/}
                                </Grid>
                            </Grid>
                        </DialogContentText>
                    </DialogContent>
                </Dialog>
            </ListItem>
            <ListItem className={classes.listItem}>
                {
                    localStorage.getItem("auth") === "login" ? (
                        <Button
                            onClick={handleClickSignOut}
                            color="white"
                            className={classes.navLink}
                        >
                            Sign Out
                        </Button>

                    ) : (
                        <Button
                            onClick={handleClickOpenRegister}
                            color="white"
                            className={classes.navLink}
                        >
                            Sign Up
                        </Button>
                    )
                }
                <Dialog
                    open={openRegister}
                    onClose={handleCloseRegister}
                    aria-labelledby="alert-dialog-title"
                    aria-describedby="alert-dialog-description"
                >
                    <DialogTitle id="alert-dialog-title" style={{ fontSize:16, textAlign: 'center', borderBottom: '1px solid rgba(34, 34, 34, 0.6)' }}>
                        <strong>Sign Up</strong>
                    </DialogTitle>
                    <DialogContent>
                        <DialogContentText id="alert-dialog-description">
                            <Grid container direction={"row"} spacing={5}>
                                <Grid item sm={12} xs={12}>
                                    <Grid item style={{ marginTop: 20 }}>
                                        <TextField onChange={handledName} className="input-login" fullWidth label="Name" variant="outlined" />
                                    </Grid>
                                    <Grid item>
                                        <TextField onChange={handledEmailRegister} className="input-login" fullWidth label="Email" variant="outlined" />
                                    </Grid>
                                    <Grid item>
                                        <TextField onChange={handledPasswordRegister} type="password" className="input-login" fullWidth label="Password" variant="outlined" />
                                    </Grid>
                                    <Grid item>
                                        <Autocomplete
                                            id="country-select-demo"
                                            options={countries}
                                            onChange={(event, newValue) => {
                                                setCountry(newValue)
                                            }}
                                            autoHighlight
                                            getOptionLabel={(option) => option.label + " +" + option.phone}
                                            renderOption={(option) => (
                                                <React.Fragment>
                                                    <span>{countryToFlag(option.code)}</span>
                                                    {option.label} ({option.code}) +{option.phone}
                                                </React.Fragment>
                                            )}
                                            renderInput={(params) => (
                                                <TextField
                                                    {...params}
                                                    label="Choose a country"
                                                    variant="outlined"
                                                    inputProps={{
                                                        ...params.inputProps,
                                                        autoComplete: 'new-password', // disable autocomplete and autofill
                                                    }}
                                                />
                                            )}
                                        />
                                    </Grid>
                                    <Grid item>
                                        <TextField onChange={handledPhone} className="input-login" fullWidth label="Phone" variant="outlined" />
                                    </Grid>
                                    <Grid item style={{ textAlign:'right' }}>
                                        <Button onClick={() => fetchRegister()} fullWidth variant="contained" color="primary">
                                            Keep going
                                        </Button>
                                    </Grid>
                                    <br/>
                                    {/*<Grid item style={{ textAlign:'center' }}>*/}
                                    {/*    <Button fullWidth variant="outlined" color="secondary">*/}
                                    {/*        Continúa con Google*/}
                                    {/*    </Button>*/}
                                    {/*</Grid>*/}
                                </Grid>
                            </Grid>
                        </DialogContentText>
                    </DialogContent>
                </Dialog>

            </ListItem>

            <ListItem className={classes.listItem}>
                <Tooltip
                    id="instagram-linkedin"
                    title="Follow us on Linkedin"
                    placement={window.innerWidth > 959 ? "top" : "left"}
                    classes={{tooltip: classes.tooltip}}
                >
                    <Button
                        href="https://www.linkedin.com/company/quicktrip-pe/"
                        target="_blank"
                        color="white"
                        className={classes.navLink}
                    >
                        <i className={classes.socialIcons + " fab fa-linkedin"}/>
                    </Button>
                </Tooltip>
            </ListItem>
            <ListItem className={classes.listItem}>
                <Tooltip
                    id="instagram-facebook"
                    title="Follow us on facebook"
                    placement={window.innerWidth > 959 ? "top" : "left"}
                    classes={{tooltip: classes.tooltip}}
                >
                    <Button
                        color="white"
                        href="https://www.facebook.com/quicktrip.pe"
                        target="_blank"
                        style={{ fontSize: 22 }}
                        className={classes.navLink}
                    >
                        <i className={classes.socialIcons + " fab fa-facebook"}/>
                    </Button>
                </Tooltip>
            </ListItem>
            <ListItem className={classes.listItem}>
                <Tooltip
                    id="instagram-tooltip"
                    title="Follow us on instagram"
                    placement={window.innerWidth > 959 ? "top" : "left"}
                    classes={{tooltip: classes.tooltip}}
                >
                    <Button
                        color="white"
                        href="https://www.instagram.com/quicktrip.pe/"
                        target="_blank"
                        className={classes.navLink}
                    >
                        <i className={classes.socialIcons + " fab fa-instagram"}/>
                    </Button>
                </Tooltip>
            </ListItem>
        </List>
    );
}
