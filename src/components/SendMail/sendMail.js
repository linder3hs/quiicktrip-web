import Swal from 'sweetalert2';
import emailjs from "emailjs-com"

const SendMail = {

    send(body) {
        var templateParams = {
            name: body.name,
            email: body.email,
            phone: body.phone,
            country: body.country,
            guide: body.guide,
            title: body.title,
        };

        emailjs.send("quicktrip", "template_4F5ga6QR", templateParams, "user_REmeYYizAYI9GmbI7KFNa")
            .then((response) => {
                const title = 'Mail successfully sent!';
                const icon = 'success';
                Swal.fire({
                    icon,
                    title,
                    showConfirmButton: false,
                    timer: 1500
                })
            console.log('SUCCESS!', response.status, response.text);
        }, (err) => {
                const title = 'There was a problem use the WhatsApp option!';
                const icon = 'error';
                Swal.fire({
                    icon,
                    title,
                    showConfirmButton: false,
                    timer: 1500
                })
            console.log('FAILED...', err);
        });
    }
};

export default SendMail;
