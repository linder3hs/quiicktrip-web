import firebase from "firebase";
import "firebase/auth"

const firebaseConfig = {
    apiKey: "AIzaSyAzuELWH1N8qQAgIlYWethf5_cfjB2bOMc",
    authDomain: "quicktrip-64366.firebaseapp.com",
    databaseURL: "https://quicktrip-64366.firebaseio.com",
    projectId: "quicktrip-64366",
    storageBucket: "quicktrip-64366.appspot.com",
    messagingSenderId: "701746141951",
    appId: "1:701746141951:web:3a9e181c2c01861c894aca",
    measurementId: "G-FXRL44SB5J"
};

firebase.initializeApp(firebaseConfig)

const storage = firebase.storage()
const auth = firebase.auth()


export  {
    storage, auth, firebase as default
}
