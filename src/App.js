import React, { useState, useContext } from "react";
import ReactDOM from "react-dom"
import {Context} from "./Context"
import { createBrowserHistory } from "history"
import { BrowserRouter, Switch, Route, Redirect } from "react-router-dom";

import "assets/scss/material-kit-react.scss?v=1.8.0"

// pages for this product
import Components from "views/Components/Components.js"
import LandingPage from "views/LandingPage/LandingPage.js"
import ProfilePage from "views/ProfilePage/ProfilePage.js"
import LoginPage from "views/LoginPage/LoginPage.js"
import Login from 'views/Login'
import Register from 'views/Register'
import ListGuide from 'views/ListGuideByCity'
import Detail from 'views/ExperienceDetail'
import About from 'views/About'
import Terms from "./views/Terms";

// Admin
import AdminLogin from 'views/LoginAdmin'
import HomeAdmin from 'views/HomeAdmin'
import CreateCity from 'views/CreateCityAdmin'
import CreateGuide from 'views/CreateGuide'
import CreateExperience from 'views/CreateExperience'
import ListAdminCity from "./views/ListAdminCity"
import ListExperience from 'views/ListExperience'
import ListAdminGuide from "./views/ListAdminGuide"
import EditCity from './views/EditAdminCity'
import EditGuide from './views/EditAdminGuide'
import EditExperience from './views/EditAdminExperience'

var hist = createBrowserHistory();

export default function App() {

    const {isAuth, activateAuth} = useContext(Context)

    return (
        <BrowserRouter>
            <Switch>
                <Route path="/landing-page" component={LandingPage} />
                <Route path="/profile-page" component={ProfilePage} />
                <Route path="/login-page" component={LoginPage} />
                {/*<Route path="/login" component={Login} />*/}
                {/*<Route path="/register" component={Register} />*/}
                <Route path="/list-guide" component={ListGuide} />
                <Route path="/detail" component={Detail} />
                <Route path="/admin" component={AdminLogin} />
                <Route path="/admin-home" component={HomeAdmin} />
                <Route path="/create-city" component={CreateCity} />
                <Route exact path="/about" component={About} />
                <Route exact path="/terms-condition" component={Terms} />
                // Admin
                <PrivateRoute path="/list-city" component={ListAdminCity} />
                <PrivateRoute path="/list-experience" component={ListExperience} />
                <PrivateRoute path="/list-admin-guide" component={ListAdminGuide} />
                <PrivateRoute path="/create-guide" component={CreateGuide} />
                <PrivateRoute path="/create-experience" component={CreateExperience} />
                <PrivateRoute path="/edit-city" component={EditCity} />
                <PrivateRoute path="/edit-guide" component={EditGuide} />
                <PrivateRoute path="/edit-experience" component={EditExperience} />
                <Route path="/" component={Components} />
            </Switch>
        </BrowserRouter>
    )

    function PrivateRoute({component: Component, ...rest}) {
        return (
            <Context.Consumer>
                {
                    ({isAuth}) => {
                        if (isAuth) {
                            return (
                                <Route
                                    {...rest}
                                    render={props =>
                                        <Component {...props} />
                                    }
                                />
                            )
                        } else {
                            return (
                                <Route
                                    {...rest}
                                    render={props =>
                                        <Redirect
                                            to={{
                                                pathname: "/admin",
                                                state: {from: props.location}
                                            }}
                                        />
                                    }
                                />
                            )
                        }
                    }
                }
            </Context.Consumer>
        )
    }

}

// ReactDOM.render(
// ,
//   document.getElementById("root")
// );
