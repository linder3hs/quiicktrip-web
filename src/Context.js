import React, { createContext, useState } from 'react'

export const Context = createContext();

const Provider = ({ children }) => {
    const [isAuth, setAuth] = useState(() => {
        return window.localStorage.getItem('auth')
    })

    const [name, setName] = useState(() => {
        return window.localStorage.getItem('name')
    })

    const value = {
        isAuth,
        name,
        activateAuth: (id, name) => {
            setAuth(true)
            setName(name)
            window.localStorage.setItem('auth', true)
            window.localStorage.setItem('id', id)
            window.localStorage.setItem('name', name)
        },
        removeAuth: () => {
            setAuth(false)
            setName('')
            window.localStorage.removeItem('auth')
            window.localStorage.removeItem('id')
            window.localStorage.removeItem('name')
        }
    }

    return(
        <Context.Provider value={value}>
            {children}
        </Context.Provider>
    )
}

export default {
    Provider,
    Consumer: Context.Consumer
}
